#include <Arduino.h>
#include <SPI.h>
#include <SD.h>

#include "Player.h"
#include "VS1053.h"
#include "RFID.h"
#include "CommandLineInterface.h"
#include "Network.h"
#include "Buttons.h"
#include "StatusLedRing.h"


#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "Main";
#endif

// ----------------------------------------------------------------------------------------
// TEST

// ----------------------------------------------------------------------------------------
    
Player                  *m_Player;
vs1053::VS1053          *m_VS1053;
RFID                    *m_RFID;
CommandLineInterface    *m_CLI;
Network                 *m_Network;
StatusLed               *m_StatusLed;
Buttons                 *m_Buttons;

void setup() {
    
    // do some basic hardware initializations
    Serial.begin(115200);

    // set CS pins high ( normally hardware dependant)
    digitalWrite(21, HIGH);
    pinMode(21, OUTPUT);

    pinMode(23,INPUT_PULLUP);
    pinMode(13,INPUT_PULLUP);

    // TODO load default pin settings or use internal memory
    SPI.begin(18, 19, 23);
    SD.begin(13);

    ESP_LOGI(TAG, "NeoEnRav v%lu.%lu.%lu", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    // create the instances for our player
    m_Player    = new Player;
    m_CLI       = new CommandLineInterface;
    m_VS1053    = new vs1053::VS1053;
    m_RFID      = new RFID;
    m_Network   = new Network;
    m_StatusLed = new StatusLedRing;
    m_Buttons   = new Buttons;


    // connect the different devices (if necessary)
    m_VS1053->Connect(&SPI, 25, 26, 32);

    m_RFID->Connect(&SPI, 21, 5);

    //prepare the CLI
    m_CLI->connect(&Serial);
    if (!m_CLI->attach(m_Player)) 
    {
        ESP_LOGE(tag, "Could not attach player to CLI");
    }
    if (!m_CLI->attach(m_Network)) 
    {
        ESP_LOGE(tag, "Could not attach network to CLI");
    }

    // prepare the player
    m_Player->Attach(m_VS1053);
    m_Player->Attach(m_RFID);    
    m_Player->Attach(m_StatusLed);
    m_Player->Attach(m_Buttons);

    //---------------------------------
    // starte the different tasks
        
    // try to start the VS1053 thread
    if (! m_VS1053->Start())
    {
        ESP_LOGW(TAG, "Start of VS1053 failed!");
    }

    // 
    if (! m_RFID->Start())
    {
        ESP_LOGW(TAG, "Start of RFID failed!");
    }

    // start the command line interface
    if (! m_CLI->Start())
    {
        ESP_LOGW(TAG, "Start of CLI failed!");
    }

    //
    if (! m_Network->Start())
    {
        ESP_LOGW(TAG, "Start of Network failed!");
    }

    //
    if (! m_StatusLed->Start())
    {
        ESP_LOGW(TAG, "Start of visualization failed!");
    }

    // watch the buttons
    if (! m_Buttons->Start())
    {
        log_d("Start of Button task failed");
    }

    // the player is the last thing we start
    if (! m_Player->Start())
    {
        ESP_LOGW(TAG, "Start of RFID failed!");
    }

    // ----------------------------------------------------------------------------------------
    //TEST
    
    // ----------------------------------------------------------------------------------------

}

// The loop function is called in an endless loop
void loop() 
{
    // todo check all other threads
    delay(1);

    // ----------------------------------------------------------------------------------------
    //TEST

    // ----------------------------------------------------------------------------------------
}