/** @file VS1053.h
 * 
 * @brief Hardware driver for VS1053 chip.
 *
 * Adapted for ESP32 (FreeRTOS).
 *
 *
 * Licensed under GNU GPLv3 <http://gplv3.fsf.org/>
 * Copyright © 2017
 *
 * @authors enrav, asealion,, maniacbug
 *
 * Development log:
 *  - 2011: initial VS1053 Arduino library
 *          originally written by J. Coliz (github: @maniacbug),
 *  - 2016: refactored and integrated into Esp-radio sketch
 *          by Ed Smallenburg (github: @edzelf)
 *  - 2019: refactored to make full use of RTOS capabilities
 *          by Mike Reinecke (github: @asealion )
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License or later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef VS1053_H
    #define VS1053_H

    #include <memory>
    #include <queue>

    #ifndef UNIT_TEST
        #include "Arduino.h"
        #include "SPI.h"
    #else
        //TODO create arduino mock
    #endif

    #ifndef UNIT_TEST
        #include "thread.hpp"
        #include "mutex.hpp"
    #else
        //TODO create freertos mock
    #endif

    #include "cbuf.h"

    #include "MediaFile.h"
    
    namespace vs1053 {

        class VS1053 : public cpp_freertos::Thread
        {
            public:

                /** @brief Set this to true to get several status messages
                 * 
                 * */
                bool        m_ModuleVerbose;

                /** @brief Set this to true (in combination with verbose) to debug the library
                 * 
                 * */
                bool        m_ModuleDebug;

            public:
                // Constructor/Destructor
                VS1053( SPIClass *pSPI = &SPI, uint8_t cs_pin = 0, uint8_t dcs_pin = 0, uint8_t dreq_pin = 0) ;
                ~VS1053();

                // sets pins correctly and prepares SPI bus.
                void        Connect(SPIClass *pSPI, uint8_t cs_pin, uint8_t dcs_pin, uint8_t dreq_pin) ; 

                bool        RegisterFileCompleteNotification(TaskHandle_t handle, uint32_t bitValue);

                // try to start the thread ( only possible if the player is configured )
                bool        Start( void );                             

                void        SetVolume( uint8_t volume );
                uint8_t     GetVolume( void );

                bool        PlaySong(std::shared_ptr<mediafile::MediaFile> mediaFile);
                bool        StopSong(void);

                uint32_t    GetChipId( void );

                const char* GetDriverVersion( void );

            protected:
            
                void        Run( void );                                // the "real" thread function


            private:

                // Serial Control Interface (SCI) Register
                const uint8_t SCI_MODE          = 0x0 ;
                const uint8_t SCI_STATUS        = 0x1 ;
                const uint8_t SCI_BASS          = 0x2 ;
                const uint8_t SCI_CLOCKF        = 0x3 ;
                const uint8_t SCI_DECODE_TIME   = 0x4 ;
                const uint8_t SCI_AUDATA        = 0x5 ;
                const uint8_t SCI_WRAM          = 0x6 ;
                const uint8_t SCI_WRAMADDR      = 0x7 ;
                const uint8_t SCI_HDAT0         = 0x8 ;
                const uint8_t SCI_HDAT1         = 0x9 ;
                const uint8_t SCI_AIADDR        = 0xA ;
                const uint8_t SCI_VOL           = 0xB ;
                const uint8_t SCI_AICTRL0       = 0xC ;
                const uint8_t SCI_AICTRL1       = 0xD ;
                const uint8_t SCI_AICTRL2       = 0xE ;
                const uint8_t SCI_AICTRL3       = 0xF ;

                // SCI_MODE bits
                const uint8_t SM_SDINEW         = 11 ;        	// Bitnumber in SCI_MODE always on
                const uint8_t SM_RESET          = 2 ;        	// Bitnumber in SCI_MODE soft reset
                const uint8_t SM_CANCEL         = 3 ;         	// Bitnumber in SCI_MODE cancel song
                const uint8_t SM_TESTS          = 5 ;         	// Bitnumber in SCI_MODE for tests
                const uint8_t SM_LINE1          = 14 ;        	// Bitnumber in SCI_MODE for Line input

                // Data
                struct Configuration 
                {                
                    SPIClass        *pSpi;

                    struct Pins
                    {
                        uint8_t         ControlChipSelect;              // Pin where CS line is connected
                        uint8_t         DataChipselect;                 // Pin where DCS line is connected
                        uint8_t         DataRequest;                    // Pin where DREQ line is connected
                    }               Pins;

                    SPISettings     Settings;                           // SPI settings for this slave

                    bool            Configured;
                }               Configuration;

                struct Player
                {
                    const uint32_t  ChunkSize = 32 ;                    // minimum number of bytes the chip could receive via SDI if dreq is high
                    const uint32_t  FifoSize  = 2048;                   // internal FIFO
                    uint8_t         EndFillByte;
                    uint32_t        ChipId;  

                    uint8_t         Volume;                             //

                
                }               Player;
                
                cbuf            Buffer;

                enum Status {
                    STATUS_IDLE,
                    STATUS_PLAYING,
                }               Status;

                cpp_freertos::MutexStandard                             FileQueueMutex; 
                std::queue<std::shared_ptr<mediafile::MediaFile>>       FileQueue;

                std::shared_ptr<mediafile::MediaFile>                   pActualFile;

                const char      VolumeTable[22]={   0,50,60,65,70,75,80,82,84,86,
                                                    88,90,91,92,93,94,95,96,97,98,99,100}; //22 elements

                struct FileComplete {
                    TaskHandle_t handle;
                    uint32_t bitValue;
                };

                std::vector<FileComplete>   FileCompleteVector;

                const char*     LibVersion = "1.0.0";

            private:

                //
                void        SelectControlInterface(bool enable);
                void        SelectDataInterface(bool enable);
                void        SelectDataInterfaceRaw(bool enable);

                uint16_t    ReadRegister( uint8_t registerNumber );
                void        WriteRegister( uint8_t registerNumber, uint16_t value );

                uint16_t    ReadRam( uint16_t address ) ;
                void        WriteRam( uint16_t address, uint16_t data ) ;

                bool        ReadChipId( void );

                void        SendSoftwareReset() ;

                void        WaitDataRequest();

                int32_t     FillInternalCircularBuffer( void );

                void        FinishSong( void ) ;

                void        SendFillBytes( size_t length );

                void        SetVolume( void );
        
        } ;
    }

#endif
