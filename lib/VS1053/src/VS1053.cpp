#include "VS1053.h"

#include "TimeElapsed.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "VS1053";
#endif


namespace vs1053 {
// Notifications
static const uint32_t NOTIFICATION__PLAY_FILE       = 0x01000000;
static const uint32_t NOTIFICATION__STOP_FILE       = 0x02000000;

static const uint32_t NOTIFICATION__DATA_REQUEST    = 0x80000000;


static xTaskHandle xVS1053Task;

//---------------------------------------------------------------------------------------
VS1053::VS1053(SPIClass *pSPI, uint8_t cs, uint8_t dcs, uint8_t dreq):
    Thread("VS1053", 10240, tskIDLE_PRIORITY),    
    Buffer(2 * Player.FifoSize),
    Status(STATUS_IDLE)
{
    m_ModuleVerbose = false;
    m_ModuleDebug   = false;

    Configuration.pSpi                      = pSPI;

    Configuration.Pins.ControlChipSelect    = cs;
    Configuration.Pins.DataChipselect       = dcs;
    Configuration.Pins.DataRequest          = dreq;

    // check if all necessary configurations are made
    if (!(Configuration.Pins.ControlChipSelect) || !(Configuration.Pins.DataChipselect) || !(Configuration.Pins.DataRequest))
    {
        Configuration.Configured = false;
    }
    else
    {
        Configuration.Configured = true;
    }

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGD(TAG, "Constructor complete");
    }
}

//---------------------------------------------------------------------------------------
VS1053::~VS1053()
{
    // destructor
    if (this->m_ModuleVerbose)
    {
        ESP_LOGD(TAG, "Constructor complete");
    }
}


//---------------------------------------------------------------------------------------
void VS1053::Connect(SPIClass *pSPI, uint8_t cs, uint8_t dcs, uint8_t dreq)
{
    Configuration.pSpi                      = pSPI;

    Configuration.Pins.ControlChipSelect    = cs;
    Configuration.Pins.DataChipselect       = dcs;
    Configuration.Pins.DataRequest          = dreq;

    Player.Volume                           = 86;

    // check if all necessary configurations are made
    if (!(Configuration.Pins.ControlChipSelect) || !(Configuration.Pins.DataChipselect) || !(Configuration.Pins.DataRequest) || !(Configuration.pSpi))
    {
        Configuration.Configured = false;
    }
    else
    {
        Configuration.Configured = true;
    }
}


//---------------------------------------------------------------------------------------
bool VS1053::RegisterFileCompleteNotification(TaskHandle_t handle, uint32_t bitValue)
{
    bool result = false;

    if (handle && bitValue)
    {
        FileCompleteVector.push_back({handle, bitValue});

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void IRAM_ATTR DreqIsr() {
    static BaseType_t xHigherPriorityTaskWoken;

    // tell the class, the pin is high
    if (xVS1053Task)
    {
        xTaskNotifyFromISR(xVS1053Task, NOTIFICATION__DATA_REQUEST, eSetBits, &xHigherPriorityTaskWoken);
    }

    if (xHigherPriorityTaskWoken)
    {
        portYIELD_FROM_ISR();
    }
}


//---------------------------------------------------------------------------------------
bool VS1053::Start()
{
    bool result = false;

    pinMode(Configuration.Pins.DataRequest, INPUT);             // DREQ is an input
    pinMode(Configuration.Pins.ControlChipSelect, OUTPUT);      // 
    pinMode(Configuration.Pins.DataChipselect, OUTPUT);

    digitalWrite(Configuration.Pins.ControlChipSelect, HIGH);   // Start HIGH for SCI en SDI
    digitalWrite(Configuration.Pins.DataChipselect, HIGH);

    attachInterrupt(Configuration.Pins.DataRequest, DreqIsr, RISING);

    // start the thread, if all necessary configurations are set
    if ((Configuration.Configured) && (Configuration.pSpi->bus()))
    {
        result = Thread::Start();

        xVS1053Task = Thread::GetHandle();

    }
    else
    {
        ESP_LOGW(TAG, "Could not start thread");

        if (!Configuration.Configured)
        {
            ESP_LOGI(TAG, "- Connection not configured");
        }

        if (!Configuration.pSpi->bus())
        {
            ESP_LOGI(TAG, "- SPI interface not ready");
        }
    }    

    return result;
}


//---------------------------------------------------------------------------------------
bool VS1053::PlaySong(std::shared_ptr<mediafile::MediaFile> mediaFile)
{
    bool result = false;

    if ((xVS1053Task) && (mediaFile))
    {
        FileQueueMutex.Lock();

        FileQueue.push(mediaFile);

        xTaskNotify(xVS1053Task, NOTIFICATION__PLAY_FILE, eSetBits);

        FileQueueMutex.Unlock();

        if (this->m_ModuleVerbose)
        {
            ESP_LOGD(TAG, "Play \"%s\"", mediaFile->getTitle());
        }

        result = true;
    }
    else
    {
        if (!xVS1053Task)
        {
            ESP_LOGE(TAG, "Task not running");
        }
        else
        {
            ESP_LOGE(TAG, "No MediaFile provided");
        }
    }

    return result;
}


//---------------------------------------------------------------------------------------
bool VS1053::StopSong(void)
{
    bool result = false;

    if ((xVS1053Task) && (Status == STATUS_PLAYING))
    {
        xTaskNotify(xVS1053Task, NOTIFICATION__STOP_FILE, eSetBits);

        if (this->m_ModuleVerbose)
        {
            ESP_LOGD(TAG, "Stop %s", pActualFile->getTitle());
        }

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void VS1053::Run()
{
    uint32_t ulNotifiedValue;

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGI(TAG, "Thread started");
    }
    
    // Init SPI in slow mode (0.2 MHz)
    Configuration.Settings = SPISettings(200000, MSBFIRST, SPI_MODE0);

    // make sure the VS1053 has enough time "boot"
    DelayMS(120);

    // Most VS1053 modules will start up in midi mode.  The result is that there is no audio
    // when playing MP3.  You can modify the board, but there is a more elegant way:
    // !!! NOT SURE IF REALLY NEEDED, but it will do no harm :) !!!
    WriteRam(0xC017, 3);                             // GPIO DDR=3
    WriteRam(0xC019, 0);                             // GPIO ODATA=0
    DelayMS(100);

    SendSoftwareReset();                                       // Do a soft reset

    // make sure the VS1053 has enough time "boot"
    DelayMS(120);

    // Switch on the analog parts
    WriteRegister(SCI_AUDATA, 44100 + 1);             // 44.1kHz + stereo

    // The next clocksetting allows SPI clocking at 5 MHz, 4 MHz is safe then.
    WriteRegister(SCI_CLOCKF, 6 << 12);               // Normal clock settings multiplyer 3.0=12.2 MHz

    //SPI Clock to 4 MHz. Now you can set high speed SPI clock.
    Configuration.Settings = SPISettings(4000000, MSBFIRST, SPI_MODE0);

    WriteRegister(SCI_MODE, _BV (SM_SDINEW) | _BV(SM_LINE1));
    
    DelayMS(10);

    //
    Player.EndFillByte = ReadRam(0x1E06) & 0x00FF;

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGD(TAG, "endFillByte is 0x%02X", Player.EndFillByte);
    }

    DelayMS(100);

    // get the Chip ID
    if (!ReadChipId())
    {
        ESP_LOGE(TAG, "Could not read Chip ID!");
    }


    while (1)
    {        
        // check for notification (Don't clear bits on entry, Clear all bits on exit, if nothing happens, we will wait for good )
        xTaskNotifyWait( pdFALSE,  ULONG_MAX, &ulNotifiedValue, portMAX_DELAY );

        // Stop play
        if (ulNotifiedValue & NOTIFICATION__STOP_FILE)
        {

            if ((m_ModuleVerbose) && (m_ModuleDebug))
            {
                ESP_LOGD(TAG, "VS1053 task -> stop request flag set");
            }

            // Playing?
            if (Status == STATUS_PLAYING)
            {

                FinishSong();

                pActualFile = nullptr;

                Status = STATUS_IDLE;

                ulNotifiedValue &= ~(NOTIFICATION__DATA_REQUEST | NOTIFICATION__PLAY_FILE);

                if (FileCompleteVector.size())
                {
                    for (std::vector<FileComplete>::iterator tasksToNotify = FileCompleteVector.begin(); tasksToNotify != FileCompleteVector.end(); ++tasksToNotify)
                    {
                        xTaskNotify(tasksToNotify->handle, tasksToNotify->bitValue, eSetBits);
                    }

                    if ((m_ModuleVerbose) && (m_ModuleDebug))
                    {
                        ESP_LOGD(TAG, "send out notifications to other tasks");
                    }
                }
            }
            // Ignore
            else
            {
                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "Stop while no song is active");
                }
            }
        }

        // Data Request
        if (ulNotifiedValue & NOTIFICATION__DATA_REQUEST)
        {
            if ((m_ModuleVerbose) && (m_ModuleDebug))
            {
                ESP_LOGD(TAG, "VS1053 task -> data request flag set");
            }

            // When we are playing, the decoder could receive more data
            if (Status == STATUS_PLAYING)
            {
                uint8_t     buffer[Player.ChunkSize];
                uint32_t    availableInBuffer = Buffer.available();

                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "VS1053 task -> playing a song (data in buffer %u)", availableInBuffer);
                }

                // send as much data as possible and available
                while(digitalRead(Configuration.Pins.DataRequest) && (availableInBuffer))
                {
                    uint32_t dataLength = Player.ChunkSize;

                    // send up to ChunkSize bytes (if available in the buffer)
                    if (availableInBuffer < dataLength)
                    {
                        dataLength = availableInBuffer;
                    }

                    // read the data from the buffer 
                    Buffer.read((char*)buffer, dataLength);
                    
                    // send the data to the chip
                    SelectDataInterfaceRaw(true);
                    SPI.writeBytes(buffer, dataLength);
                    SelectDataInterfaceRaw(false);

                    // reduce the available data amount
                    availableInBuffer -= dataLength;
                };
                
                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "VS1053 task -> data left in buffer %u", availableInBuffer);
                }

                // check buffer and reload 
                if (FillInternalCircularBuffer() <= 0)
                {
                    // No more data from SD Card
                    if (m_ModuleVerbose)
                    {
                        ESP_LOGD(TAG, "VS1053 task -> end of mp3file %s", pActualFile->getTitle());
                    }

                    // TODO make sure we let the Buffer run out before we stop

                    xTaskNotify(xVS1053Task, NOTIFICATION__STOP_FILE, eSetBits);                    
                }
            }
            // Ignore
            else
            {
                // nothing to do
                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "Data Request but not playing");
                }
            }
        }

        // Play file?
        if (ulNotifiedValue & NOTIFICATION__PLAY_FILE)
        {

            if ((m_ModuleVerbose) && (m_ModuleDebug))
            {
                ESP_LOGD(TAG, "VS1053 task -> play file request flag set");
            }

            if (Status == STATUS_PLAYING) 
            {
                FinishSong();

                pActualFile = nullptr;

                Status = STATUS_IDLE;
            }

            // IDLE -> Start
            if (Status == STATUS_IDLE)
            {
                if (pActualFile == nullptr)
                {
                    int32_t bytesTransferredToBuffer;

                    FileQueueMutex.Lock();

                    pActualFile = FileQueue.front();
                    FileQueue.pop();

                    FileQueueMutex.Unlock();

                    bytesTransferredToBuffer = FillInternalCircularBuffer();

                    if (bytesTransferredToBuffer)
                    {
                        SetVolume();

                        xTaskNotify(xVS1053Task, NOTIFICATION__DATA_REQUEST, eSetBits);

                        Status = STATUS_PLAYING;
                    }
                    else
                    {
                        pActualFile = nullptr;

                        ESP_LOGW(TAG, "Initial buffer transaction failed with %d bytes", bytesTransferredToBuffer);
                    }
                }
                else
                {
                    ESP_LOGE(TAG, "Start Song without valid Media File");
                }
            }
            // Playing
            else
            {
                ESP_LOGI(TAG, "Play file while playing");
            }
        }
    }
}


//---------------------------------------------------------------------------------------
void VS1053::SendSoftwareReset()
{
    WriteRegister(SCI_MODE, _BV (SM_SDINEW) | _BV(SM_RESET));
    delay(10);
}


//---------------------------------------------------------------------------------------
int32_t VS1053::FillInternalCircularBuffer( void )
{
    int32_t result = EOF;

    if (pActualFile != nullptr)
    {
        int32_t bytesAvailableInFile = pActualFile->available();;                    

        if ((m_ModuleVerbose) && (m_ModuleDebug))        
        {
            ESP_LOGD(TAG, "remaining file length %u title \"%s\"", bytesAvailableInFile, pActualFile->getTitle());
        }
                    
        if (bytesAvailableInFile > 0)
        {
            uint32_t    roomInCircularBuffer = Buffer.room();
            uint8_t     inputBuffer[roomInCircularBuffer];
            int32_t     bytesReadFromFile;

            // limit the bytes to read to the buffer space
            if (bytesAvailableInFile > roomInCircularBuffer)
            {
                bytesAvailableInFile = roomInCircularBuffer -1 ;             
            }

            bytesReadFromFile = pActualFile->read(inputBuffer, bytesAvailableInFile);

            if ((m_ModuleVerbose) && (m_ModuleDebug))            
            {
                ESP_LOGD(TAG, "read %ld byte from media file (available %lu)", bytesReadFromFile, bytesAvailableInFile);
            }

            if (Buffer.write((char *)inputBuffer, bytesReadFromFile) != bytesReadFromFile)
            {
                ESP_LOGW(TAG, "unable to write %d byte to circular buffer", bytesReadFromFile);
            }

            result = bytesReadFromFile;
        }
    }
    else
    {
        ESP_LOGE(TAG, "tried to fill buffer without valid file object");
    }

    return result;
}


//---------------------------------------------------------------------------------------
void VS1053::SetVolume( uint8_t volume )
{
    // Set volume.  Both left and right.
    // Input value is 0..21.  21 is the loudest.
    // Clicking reduced by using 0xf8 to 0x00 as limits.

    if(volume > 21)
    {
        ESP_LOGW(TAG, "Volume %u is to high. Only values between 0-21 are allowed", volume);
        volume=21;
    }

    volume = VolumeTable[volume];

    if(volume != Player.Volume)
    {
        Player.Volume=volume;

        SetVolume();

        if (m_ModuleVerbose)
        {
            ESP_LOGD(TAG, "Set Volume to %04x", Player.Volume);
        }
    }
}


//---------------------------------------------------------------------------------------
void VS1053::SetVolume(void)
{
    uint16_t value = map(Player.Volume, 0, 100, 0xF8, 0x00);
    
    value=(value << 8) | value;
    
    WriteRegister(SCI_VOL, value);                  // Volume left and right
}


//---------------------------------------------------------------------------------------
uint8_t VS1053::GetVolume( void )
{
    return Player.Volume;
}


//---------------------------------------------------------------------------------------
void VS1053::SelectControlInterface(bool enable)
{
    if (enable)
    {
        // check if we could send data
        if (!digitalRead(Configuration.Pins.DataRequest))
        {
            // wait for the pin to go high
            WaitDataRequest();
        }

        // drain the semaphore
        xTaskNotifyWait( pdFALSE, NOTIFICATION__DATA_REQUEST, NULL, 0);

        SPI.beginTransaction(Configuration.Settings);           // Prevent other SPI users
        digitalWrite(Configuration.Pins.ControlChipSelect, LOW);
    }
    else 
    {
        digitalWrite(Configuration.Pins.ControlChipSelect, HIGH);
        Configuration.pSpi->endTransaction();                       // Allow other SPI users
    }
}


//---------------------------------------------------------------------------------------
void VS1053::SelectDataInterface(bool enable)
{
    if (enable)
    {
        // check if we could send data
        if (!digitalRead(Configuration.Pins.DataRequest))
        {
            // wait for the pin to go high
            WaitDataRequest();
        }

        // drain the semaphore
        xTaskNotifyWait( pdFALSE, NOTIFICATION__DATA_REQUEST, NULL, 0);


        SPI.beginTransaction(Configuration.Settings);           // Prevent other SPI users
        digitalWrite(Configuration.Pins.DataChipselect, LOW);
    }
    else 
    {
        digitalWrite(Configuration.Pins.DataChipselect, HIGH);
        Configuration.pSpi->endTransaction();                       // Allow other SPI users

        // wait
        if (!digitalRead(Configuration.Pins.DataRequest))
        {
            // wait for the pin to go high
            WaitDataRequest();
        }
    }
}


//---------------------------------------------------------------------------------------
void VS1053::SelectDataInterfaceRaw(bool enable)
{
    if (enable)
    {
        SPI.beginTransaction(Configuration.Settings);           // Prevent other SPI users
        digitalWrite(Configuration.Pins.DataChipselect, LOW);
    }
    else 
    {
        digitalWrite(Configuration.Pins.DataChipselect, HIGH);
        Configuration.pSpi->endTransaction();                       // Allow other SPI users
    }
}


// //---------------------------------------------------------------------------------------
uint16_t VS1053::ReadRegister( uint8_t registerNumber )
{
    uint16_t result = 0;

    SelectControlInterface(true);                               // aquire the SPI bus

    Configuration.pSpi->write(3);                               // read operation
    Configuration.pSpi->write(registerNumber);                  // register to read (0..0xF)


    result =  (Configuration.pSpi->transfer(0xFF) << 8);        // Read 16 bits data
    result += (Configuration.pSpi->transfer(0xFF));  

    SelectControlInterface(false);                              // release the SPI bus

    return result;
}


//---------------------------------------------------------------------------------------
void VS1053::WriteRegister( uint8_t registerNumber, uint16_t value )
{
    SelectControlInterface(true);                               // aquire the SPI bus

    Configuration.pSpi->write(2);                               // write operation

    Configuration.pSpi->write(registerNumber);                  // register to write (0..0xF)
    Configuration.pSpi->write16(value);                         // send 16 bits data

    SelectControlInterface(false);                              // release the SPI bus    
}


//---------------------------------------------------------------------------------------
uint16_t VS1053::ReadRam( uint16_t address ) 
{
    WriteRegister(SCI_WRAMADDR, address);       // Start reading from WRAM
    return ReadRegister(SCI_WRAM);              // Read back result
}


//---------------------------------------------------------------------------------------
void VS1053::WriteRam( uint16_t address, uint16_t data ) 
{
    WriteRegister(SCI_WRAMADDR, address);
    WriteRegister(SCI_WRAM, data);
}


//---------------------------------------------------------------------------------------
void VS1053::WaitDataRequest()
{
    uint32_t notification;

    while(!digitalRead(Configuration.Pins.DataRequest))
    {
        // if we have to wait mor than 200ms
        if ((xTaskNotifyWait( pdFALSE, NOTIFICATION__DATA_REQUEST, &notification, pdMS_TO_TICKS(200)) == pdFALSE) || (notification & NOTIFICATION__DATA_REQUEST))
        {
            break;
        }
    }
}


//---------------------------------------------------------------------------------------
void VS1053::SendFillBytes(size_t len)
{
    size_t chunk_length;                         // Length of chunk 32 byte or shorter

    
    while(len)                                   // More to do?
    {
        chunk_length=len;
        if (len > Player.ChunkSize)
        {
            chunk_length = Player.ChunkSize;
        }
        len-=chunk_length;

        SelectDataInterface(true);
        while (chunk_length--)
        {            
            SPI.write(Player.EndFillByte);
        }
        SelectDataInterface(false);
    }
}


//---------------------------------------------------------------------------------------
 void VS1053::FinishSong()
{
    uint16_t Register_Mode;

    // turn down the volume
    WriteRegister(SCI_VOL, 0xFFFF);

    SendFillBytes(2052);

    DelayMS(10);

    WriteRegister(SCI_MODE, _BV (SM_SDINEW) | _BV(SM_CANCEL));

    TickType_t timestamp = xTaskGetTickCount();

    do 
    {
        SendFillBytes(32);

        Register_Mode = ReadRegister(SCI_MODE);

        DelayMS(16);   
    }
    while ((Register_Mode & _BV(SM_CANCEL)) && (TimeElapsed(timestamp) < pdMS_TO_TICKS(1000)));

    if (Register_Mode & _BV(SM_CANCEL))
    {
        SendSoftwareReset();

        DelayMS(1000);
        
        ESP_LOGW(TAG, "Song stopped incorrectly!");
    }
    else
    {
        SendFillBytes(2052);

        if (m_ModuleVerbose)
        {
            ESP_LOGD(TAG, "Song stopped correctly after %d msec", TimeElapsed(timestamp));
        }
    }

    //clear the ringbuffer    
    Buffer.flush();    
}


//---------------------------------------------------------------------------------------
bool VS1053::ReadChipId( void )
{
    bool result = true;
    uint16_t reg1;
    uint16_t reg2;


    reg1=ReadRam(0x1E00);
    reg2=ReadRam(0x1E01);

    // all high?, seems not connected
    if((reg1==0xFFFF)&&(reg2==0xFFFF))
    {
        reg1=0; 
        reg2=0;

        result = false;
    }

    Player.ChipId = (reg1 << 16) + reg2;

    if (m_ModuleVerbose)
    {
        ESP_LOGD(TAG, "chipID = 0x%08lx", Player.ChipId);
    }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t VS1053::GetChipId( void )
{
    return Player.ChipId;
}


//---------------------------------------------------------------------------------------
const char* VS1053::GetDriverVersion( void )
{
    return LibVersion;
}

}
