#ifndef MEDIAFILE_H
    #define MEDIAFILE_H

    #include <stdint.h>

    namespace mediafile {

        class MediaFile
        {
            public:
                virtual ~MediaFile(){};

                virtual int32_t available( void ) = 0;

                virtual int32_t read(uint8_t *pBuffer, uint32_t size) = 0;

                virtual const char* getTitle(void) = 0;

            private:

        };

    }
#endif