#ifndef TIME_ELAPSED__H
    #define TIME_ELAPSED__H

    #include "Arduino.h"

    #ifdef __cplusplus 
        extern "C" 
        {
    #endif

        uint32_t TimeElapsed(uint32_t TimeStamp);

    #ifdef __cplusplus 
        }
    #endif

#endif