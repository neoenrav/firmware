
#include "TimeElapsed.h"


uint32_t TimeElapsed(uint32_t TimeStamp)
{
    uint32_t result;
    uint32_t actualTimeStamp = millis();

	if (TimeStamp > actualTimeStamp) {
		result = UINT32_MAX - TimeStamp + actualTimeStamp;
	} else {
		result = actualTimeStamp - TimeStamp;
	}

	return result;
}