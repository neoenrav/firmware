#ifndef FORMAT_BYTES__H
    #define FORMAT_BYTES__H

    #include "Arduino.h"

    #ifdef __cplusplus 
        extern "C" 
        {
    #endif

        String formatBytes(size_t bytes);

    #ifdef __cplusplus 
        }
    #endif

#endif