#ifndef MP3_FILE__H
    #define MP3_FILE__H

    #include <stdint.h>

    #include "Arduino.h"
    #include "FS.h"

    #include "MediaFile.h"

    class MP3File : public mediafile::MediaFile
    {
        public:
            bool m_verbose;
            
        public:
            MP3File();

            ~MP3File();

            int32_t open(String path);

            int32_t available( void );

            bool seek(uint32_t position);

            uint32_t position( void );

            uint32_t size( void );

            int32_t read(uint8_t *pBuffer, uint32_t size);

            const char* getTitle(void);

        private:

            File    mp3File;

    };
    
#endif