#include "Mp3File.h"

#include "Arduino.h"
#include "SD.h"


#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "MP3 FILE";
#endif

MP3File::MP3File() :    
    m_verbose(false),
    mp3File()
{
}

MP3File::~MP3File()
{
    mp3File.close();    
}

int32_t MP3File::open(String path)
{
    int32_t size = -1;

    if (mp3File)
    {
        mp3File.close();
    }

    //try to open the new file
    mp3File = SD.open(path);

    if (mp3File)
    {
        size = mp3File.size();

        mp3File.seek(size-(size * 0.25));

        if (m_verbose)
        {
            log_d("open file: \"%s\" (%lu)kByte", mp3File.name(), size/1024);
        }
    }
    else
    {
        log_e("cold not open file \"%s\"", path.c_str());
    }
    

    return size;    
}


bool MP3File::seek(uint32_t position)
{
    bool result = false;

    if (mp3File)
    {
        result = mp3File.seek(position);
    }

    return result;
}


uint32_t MP3File::position( void )
{
    size_t result = 0;

    if (mp3File)
    {
        result = mp3File.position();
    }

    return result;
}

uint32_t MP3File::size( void )
{
    size_t result = 0;

    if (mp3File)
    {
        result = mp3File.size();
    }

    return result;
}


int32_t MP3File::read(uint8_t *pBuffer, uint32_t size)
{
    int32_t result = EOF;

    if (mp3File)
    {
        result = mp3File.read(pBuffer, size);
    }

    return result;
}

int32_t MP3File::available( void )
{
    int32_t result = EOF;

    if (mp3File)
    {
        result = mp3File.available();
    }

    return result;
}

const char* MP3File::getTitle(void)
{
    return mp3File.name();
}


// //---------------------------------------------------------------------------------------
// bool VS1053::connecttoSD(String sdfile, bool resume)
// {
//     const uint8_t ascii[60]={
//           //196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215,   ISO
//             142, 143, 146, 128, 000, 144, 000, 000, 000, 000, 000, 000, 000, 165, 000, 000, 000, 000, 153, 000, //ASCII
//           //216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235,   ISO
//             000, 000, 000, 000, 154, 000, 000, 225, 133, 000, 000, 000, 132, 143, 145, 135, 138, 130, 136, 137, //ASCII
//           //236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255    ISO
//             000, 161, 140, 139, 000, 164, 000, 162, 147, 000, 148, 000, 000, 000, 163, 150, 129, 000, 000, 152};//ASCII

//     char path[256];
//     uint16_t i=0, s=0;
//     uint32_t position = 0;
//     uint32_t playlist = 0;
//     String fileExtension;
//     bool result = false;
//     EventBits_t     bitFlags = 0;

//     stop_mp3client();                           // Disconnect if still connected
//     clientsecure.stop();                        // release memory if allocated
//     clientsecure.flush(); 

//     m_f_localfile=true;
//     m_f_webstream=false;

//     while(sdfile[i] != 0){                      //convert UTF8 to ASCII
//         path[i]=sdfile[i];
//         if(path[i] > 195){
//             s=ascii[path[i]-196];
//             if(s!=0) path[i]=s;                 // found a related ASCII sign
//         } i++;
//     }
//     path[i]=0;
//     ESP_LOGD(TAG, "Reading file: %s", path);

//     // get the file extension
//     fileExtension = sdfile.substring(sdfile.lastIndexOf('.') + 1, sdfile.length());

//     ESP_LOGV(TAG, "Play File with Extension \"%s\"", fileExtension.c_str());

//     //
//     fs::FS &fs=SD;

//     if ((resume) && (fs.exists(sdfile.substring(0,sdfile.length() - 4) + ".pos")) ) 
//     {
//         File myTempFile;

//         ESP_LOGV(TAG, "Resuming track...");
//         myTempFile = fs.open(sdfile.substring(0,sdfile.length() - 4) + ".pos");

//         if (myTempFile) 
//         {
//             if (myTempFile.find("File Position:")) 
//             {
//                 position = myTempFile.parseInt();

//                 ESP_LOGD(TAG, "Resume playing at position %u", position);
//             } 
//             else
//             {
//                 ESP_LOGD(TAG, "Position Identification not found");
//             }

//             myTempFile.seek(0);
            
//             if (myTempFile.find("Playlist:")) 
//             {
//                 playlist = myTempFile.parseInt();

//                 ESP_LOGD(TAG, "Resume playlist at entry %u", playlist);
//             } 
//             else
//             {
//                 ESP_LOGD(TAG, "Playlist Identification not found");
//             }

//             myTempFile.close();
//         }
//     }


//     if (fileExtension.equalsIgnoreCase("MP3"))
//     {
//         m_mp3title=sdfile.substring(sdfile.lastIndexOf('/') + 1, sdfile.length());

//         result = openMp3File(path, position);

//         bitFlags = SF_PLAYING_FILE;
//     }
//     else if (fileExtension.equalsIgnoreCase("M3U"))
//     {
//         // save the playlist path and start with the resumed entry
//         m_playlist      = path;
//         m_playlist_num  = playlist;

//         String actualEntry = findNextPlaylistEntry(true);

//         //send the file to the player
//         if(actualEntry.substring(actualEntry.lastIndexOf('.') + 1, actualEntry.length()).equalsIgnoreCase("mp3"))
//         {
//             ESP_LOGI(TAG, "Playing Entry from playlist \"%s\"", actualEntry.c_str());

//             m_mp3title=actualEntry.substring(actualEntry.lastIndexOf('/') + 1, actualEntry.length());

//             result = openMp3File(actualEntry, position);
//         } 
//         else 
//         {
//             ESP_LOGW(TAG, "Invalid Entry from playlist \"%s\"", actualEntry.c_str());
//         }

//         bitFlags = SF_PLAYING_FILE | SF_PLAYING_AUDIOBOOK;
//     }

//     if ((result) && (m_SystemFlagGroup))
//     {
//         xEventGroupSetBits(m_SystemFlagGroup, bitFlags);

//         showstreamtitle(m_mp3title.c_str(), true);
//     }

//     return result;
// }