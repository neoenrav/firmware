#include "Arduino.h"

#include "Buttons.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "RFID";
#endif


//---------------------------------------------------------------------------------------
Buttons::Buttons():
    Thread("Buttons", 4096, tskIDLE_PRIORITY),
    m_ModuleVerbose(false),
    m_ModuleDebug(false),
    m_pVolUpButton(NULL),
    m_VolUpLongPressed(false),
    m_pVolDownButton(NULL),
    m_VolDownLongPressed(false),
    m_pPauseButton(NULL),
    m_PauseLongPressed(false)
{

    //create teh buttons
    m_pVolUpButton      = new Button(34);
    m_pVolDownButton    = new Button(35);
    m_pPauseButton      = new Button(33);


    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGD(TAG, "Constructor complete");
    }
}


//---------------------------------------------------------------------------------------
Buttons::~Buttons()
{
}


//---------------------------------------------------------------------------------------
bool Buttons::Start( void )
{
    bool result = false;
    

    m_pVolUpButton->begin();
    m_pVolDownButton->begin();
    m_pPauseButton->begin();

    result = Thread::Start();

    return result;
}


//---------------------------------------------------------------------------------------
void Buttons::Run( void )
{   	

    while (1)
    {

        if (m_pPauseButton)
        {
            m_pPauseButton->read();

            if (m_pPauseButton->pressedFor(1000) && (!m_PauseLongPressed))
            {
                m_PauseLongPressed = true;

                sendEvent(ButtonEvents::RESET);

                if (m_ModuleVerbose)
                {
                    log_d("Pause/Resume/Reset long press");
                }
            }

            if (m_pPauseButton->wasReleased())
            {
                if (!m_PauseLongPressed)
                {
                    sendEvent(ButtonEvents::PAUSE);

                    if (m_ModuleVerbose)
                    {
                        log_d("Pause/Resume/Reset short press");
                    }
                }
                m_PauseLongPressed = false;
            }
        }


        if (m_pVolDownButton)
        {
            m_pVolDownButton->read();

            if (m_pVolDownButton->pressedFor(1000) && (!m_VolDownLongPressed))
            {
                m_VolDownLongPressed = true;

                sendEvent(ButtonEvents::VOLUME_DOWN);

                if (m_ModuleVerbose)
                {
                    log_d("Volume Down long press");
                }
            }

            if (m_pVolDownButton->wasReleased())
            {
                if (!m_VolDownLongPressed)
                {
                    sendEvent(ButtonEvents::VOLUME_DOWN);

                    if (m_ModuleVerbose)
                    {
                        log_d("Volume Down short press");
                    }
                }

                m_VolDownLongPressed = false;
            }
        }

        if (m_pVolUpButton)
        {
            m_pVolUpButton->read();

            if (m_pVolUpButton->pressedFor(1000) && (!m_VolUpLongPressed))
            {
                m_VolUpLongPressed = true;

                sendEvent(ButtonEvents::VOLUME_UP);

                if (m_ModuleVerbose)
                {
                    log_d("Volume Up long press");
                }
            }

            if (m_pVolUpButton->wasReleased())
            {
                if (!m_VolUpLongPressed)
                {
                    sendEvent(ButtonEvents::VOLUME_UP);

                    if (m_ModuleVerbose)
                    {
                        log_d("Volume Up short press");
                    }
                }

                m_VolUpLongPressed = false;
            }
        }

        DelayMS(125);
    }
}


//---------------------------------------------------------------------------------------
bool Buttons::registerButtonEvent(ButtonEvents event, TaskHandle_t task, uint32_t mask)
{
    bool result = false;

    if (task && mask)
    {
        m_ButtonPressVector.push_back({event, task, mask});

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
bool Buttons::sendEvent(ButtonEvents event)
{
    bool result = false;

    for (std::vector<ButtonEventFlag>::iterator eventToSend = m_ButtonPressVector.begin(); eventToSend != m_ButtonPressVector.end(); ++eventToSend)
    {
        if (eventToSend->event == event)
        {
            xTaskNotify(eventToSend->handle, eventToSend->bitValue, eSetBits);

            if (m_ModuleVerbose)
            {
                log_d("Send Event %u", event);
            }

            result = true;
        }
    }

    return result;
}