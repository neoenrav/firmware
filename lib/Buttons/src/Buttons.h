#ifndef LIB_BUTTONS_SRC_BUTTONS_H_
    #define LIB_BUTTONS_SRC_BUTTONS_H_

    #include <memory>
    #include <vector>
    #include "thread.hpp"

    #include "JC_Button.h"


    /*! \brief Handles the communication with the RFID Module
        *
        *  Detailed description starts here.
        */
    class Buttons : public cpp_freertos::Thread
    {
        public:
            enum class ButtonEvents { 
                PAUSE,
                VOLUME_UP, 
                VOLUME_DOWN,
                RESET, 
            };


            /** @brief Set this to true to get several status messages
             * 
             * */
            bool        m_ModuleVerbose;

            /** @brief Set this to true (in combination with verbose) to debug the library
             * 
             * */
            bool        m_ModuleDebug;


        public:
            Buttons( void );
            ~Buttons();

            // try to start the thread ( only possible if it is configured/connected )
            bool        Start( void );   


            /** @brief
             * 
             * */
            bool        registerButtonEvent(ButtonEvents event, TaskHandle_t task, uint32_t mask);

        protected:
        
            void        Run( void );                                // the "real" thread function

        private:

            Button      *m_pVolUpButton;
            bool         m_VolUpLongPressed;
            Button      *m_pVolDownButton;
            bool         m_VolDownLongPressed;
            Button      *m_pPauseButton;
            bool         m_PauseLongPressed;

            struct ButtonEventFlag {
                ButtonEvents event;
                TaskHandle_t handle;
                uint32_t bitValue;
            };

            std::vector<ButtonEventFlag>   m_ButtonPressVector;

        private:

            bool sendEvent(ButtonEvents event);
    };


#endif // LIB_BUTTONS_SRC_BUTTONS_H_
