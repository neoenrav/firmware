#include "PlayObjectFolder.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "PlayObjSingle";
#endif


//---------------------------------------------------------------------------------------
PlayObjectFolder::PlayObjectFolder()
{     
}


//---------------------------------------------------------------------------------------
PlayObjectFolder::~PlayObjectFolder()
{    
}


//---------------------------------------------------------------------------------------
bool PlayObjectFolder::initialize(const String& jsonFilename, const JsonObject& jsonObj)
{
    bool result = false;

    // restore the
    m_ObjectPath    = jsonObj["Path"] | "";

    m_Resume        = (jsonObj["Resume"] == "TRUE")?true:false;
    m_Repeat        = (jsonObj["Repeat"] == "TRUE")?true:false;

    m_ListPosition  = 0; // a single file has no list :)
    m_FilePosition  = jsonObj["File Position"] | 0; // if resume is not set, we will handle this when the music file is requested

    if (m_ObjectPath.length())
    {
        // remember the filename 
        CardObject::setJsonFileName(jsonFilename);

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
bool PlayObjectFolder::create(const String& filepath, bool repeat, bool resume, String jsonPath)
{
    bool result = false;

    if ((filepath.length()) && (filepath[0] == '/') && (filepath.substring(filepath.length()-4).equalsIgnoreCase(".mp3")))
    {
        if (jsonPath.length())
        {
            // check same path basics
            if ((jsonPath[0] == '/') && (jsonPath.substring(jsonPath.length()-4).equalsIgnoreCase(".json")))
            {
                CardObject::setJsonFileName(jsonPath);
            }
            else
            {
                goto Finish;
            }
        } 
        else
        {
            CardObject::setJsonFileName(filepath + ".json");
        }

        m_ObjectPath    = filepath;

        m_Resume        = resume;
        m_Repeat        = repeat;

        // start always at the beginning
        m_FilePosition  = 0;
        m_ListPosition  = 0;

        result = true;
    }

Finish:
    return result;
}


//---------------------------------------------------------------------------------------
bool PlayObjectFolder::save(const JsonObject& jsonObj)
{
    bool result = false;

    if  (jsonObj)
    {
        // if we have a file that has not already ended
        if ((m_ActualFile) && (m_ActualFile->position() < m_ActualFile->size()))
        {
            jsonObj["File Position"]    = m_ActualFile->position();
        }
        else
        {
            jsonObj["File Position"]    = 0;
        }

        jsonObj["Resume"]           = m_Resume?"TRUE":"FALSE";
        jsonObj["Repeat"]           = m_Repeat?"TRUE":"FALSE";

        jsonObj["Path"]             = m_ObjectPath;

        result =true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<mediafile::MediaFile> PlayObjectFolder::GetNextFile()
{
    std::shared_ptr<MP3File> nextFileObject = nullptr;

    // // make sure we do not play more files than available
    // if ((m_ListPosition < 1) || (m_Repeat))
    // {

    //     m_ListPosition++;

    //     // if a valid path and file os available create a single MP3 object and return it
    //     if (m_ObjectPath.length())
    //     {
    //         nextFileObject = std::make_shared<MP3File>(m_ObjectPath);

    //         // if we successfully created an object, remember it, so we could manipulate it later
    //         if (nextFileObject)
    //         {
    //             // check the resume flag and seek for the right position
    //             if ((m_ListPosition <= 1) && (m_Resume) && (m_FilePosition < nextFileObject->position()))
    //             {
    //                 nextFileObject->seek(m_FilePosition);
    //             }
    //             else
    //             {
    //                 m_FilePosition = 0;
    //             }

    //             m_ActualFile = nextFileObject;
    //         }
    //     }
    // }

    return nextFileObject;
}


//---------------------------------------------------------------------------------------
uint32_t PlayObjectFolder::getTracksRemaining( void )
{
    uint32_t result = 0;

    if (m_Repeat)
    {
        result = 1;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void PlayObjectFolder::Reset( ResetType type )
{    
    if ( type == ResetType::RESET_FILE)
    {
        if (m_ActualFile)
        {
            m_FilePosition = 0;
            m_ActualFile->seek(0);
        }

        m_ListPosition = 0;
    }
}


//---------------------------------------------------------------------------------------
uint32_t PlayObjectFolder::getTrackProgress( void )
{
    return 50;
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectFolder::getPlayListProgress( void )
{
    return 50;
}

//---------------------------------------------------------------------------------------
bool PlayObjectFolder::getRandomOrder( void )
{
    return false;
}

//---------------------------------------------------------------------------------------
void PlayObjectFolder::setRandomOrder( bool random )
{
}

 //---------------------------------------------------------------------------------------
bool PlayObjectFolder::getRepeat( void )
{
    return m_Repeat;   
}

//---------------------------------------------------------------------------------------
void PlayObjectFolder::setRepeat( bool repeat )
{
    m_Repeat = repeat;
}

//---------------------------------------------------------------------------------------
bool PlayObjectFolder::getResume( void )
{
    return m_Resume;
}

//---------------------------------------------------------------------------------------
void PlayObjectFolder::setResume( bool resume )
{
    m_Resume = resume;
}