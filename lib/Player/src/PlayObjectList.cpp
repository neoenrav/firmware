#include "PlayObjectList.h"

#include <algorithm>

#include "SD.h"

#ifdef ARDUINO_ARCH_ESP32
#include "esp32-hal-log.h"
#else
static const char *TAG = "PlayObjList";
#endif

//---------------------------------------------------------------------------------------
PlayObjectList::PlayObjectList() : m_Verbose(false),
                                    m_fs(&SD),
                                    m_ObjectPath("")
{
}

//---------------------------------------------------------------------------------------
PlayObjectList::~PlayObjectList()
{
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::initialize(const String &jsonFilename, const JsonObject &jsonObj)
{
    bool result = false;

    // restore the
    m_ObjectPath = jsonObj["Path"] | "";

    m_Resume = (jsonObj["Resume"] == "TRUE") ? true : false;
    m_Repeat = (jsonObj["Repeat"] == "TRUE") ? true : false;
    m_Random = (jsonObj["Random"] == "TRUE") ? true : false;

    // check if the resume flag is set
    if (m_Resume)
    {
        m_NextListPosition = jsonObj["List Position"] | 0;  //
        m_FileStartPosition = jsonObj["File Position"] | 0; //
    }
    else
    {
        m_NextListPosition = 0;  //
        m_FileStartPosition = 0; //
    }

    // remember the filename
    if (m_ObjectPath.length())
    {
        CardObject::setJsonFileName(jsonFilename);

        result = loadList();
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::create(const String &filepath, bool repeat, bool resume, bool random, String jsonPath)
{
    bool result = false;

    if ((filepath.length()) && (filepath[0] == '/') && (filepath.substring(filepath.length() - 4).equalsIgnoreCase(".m3u")))
    {
        if (jsonPath.length())
        {
            // check some path basics
            if ((jsonPath[0] == '/') && (jsonPath.substring(jsonPath.length() - 4).equalsIgnoreCase(".json")))
            {
                CardObject::setJsonFileName(jsonPath);
            }
            else
            {
                goto Finish;
            }
        }
        else
        {
            CardObject::setJsonFileName(filepath + ".json");
        }

        m_ObjectPath = filepath;

        m_Resume = resume;
        m_Repeat = repeat;
        m_Random = random;

        // start always at the beginning
        m_FileStartPosition = 0;
        m_NextListPosition = 0;

        result = loadList();

        result = true;
    }
    else
    {
        if (m_Verbose)
        {
            log_d("Invalid file \"%s\" to create object, need /*.m3u", filepath.c_str());
        }
    }

Finish:
    return result;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::save(const JsonObject &jsonObj)
{
    bool result = false;

    if (m_ObjectPath.length())
    {
        if (jsonObj)
        {
            // if we have a file that has not already ended
            if ((m_ActualFile) && (m_ActualFile->position() < m_ActualFile->size()))
            {
                jsonObj["File Position"] = m_ActualFile->position();

                // make sure we do not save a negative list position
                if (m_NextListPosition)
                {
                    jsonObj["List Position"] = m_NextListPosition - 1;
                }
                else
                {
                    jsonObj["List Position"] = 0;
                }
            }
            else
            {
                jsonObj["File Position"] = 0;
                jsonObj["List Position"] = m_NextListPosition;
            }

            jsonObj["Resume"] = m_Resume ? "TRUE" : "FALSE";
            jsonObj["Repeat"] = m_Repeat ? "TRUE" : "FALSE";
            jsonObj["Random"] = m_Random ? "TRUE" : "FALSE";

            jsonObj["Path"] = m_ObjectPath;

            result = true;

            if (m_Verbose)
            {
                log_d("Save actual state for %s", m_ObjectPath.c_str());
            }
        }
        else
        {
            log_e("JSON object not provided");
        }
    }
    else
    {
        log_e("PlayObject List was not initialized");
    }

    return result;
}

//---------------------------------------------------------------------------------------
std::shared_ptr<mediafile::MediaFile> PlayObjectList::getMediaFile(void)
{
    std::shared_ptr<MP3File> nextMediaObject = nullptr;

    if (m_List.size())
    {

        if (m_Verbose)
        {
            log_d("Next Track requested: Position %u of %u", (m_NextListPosition + 1), m_List.size());
        }

        //check if the file is already finished ( this could happen is the file was paused)
        if ((m_ActualFile) && (m_ActualFile->position() < m_ActualFile->size()))
        {
            nextMediaObject = m_ActualFile;

            if (m_Verbose)
            {
                log_d("Return previously loaded file \"%s\" Position %u Size %u", m_ActualFile->getTitle(), m_ActualFile->position(), m_ActualFile->size());
            }
        }
        else
        {
            // handle the repeat flag (if necessary)
            if ((m_NextListPosition == m_List.size()) && (m_Repeat))
            {
                m_NextListPosition = 0;

                // 
                if (m_Random)
                {
                    std::random_shuffle(m_List.begin(), m_List.end());
                }
            }

            // make sure we do not play more files than available
            if (m_NextListPosition < m_List.size())
            {
                // if a valid path and file is available create a single MP3 object and return it
                if (m_List.at(m_NextListPosition).length())
                {
                    nextMediaObject = std::make_shared<MP3File>();

                    if (nextMediaObject)
                    {
                        // try to initialize the MP3 file with the filename
                        while ((nextMediaObject->open(m_List.at(m_NextListPosition)) <= 0) && (m_NextListPosition < m_List.size()))
                        {
                            log_w("Failed to open file or file is empty \"%s\"", m_List.at(m_NextListPosition).c_str());

                            // try the next file
                            m_NextListPosition++;
                        }

                        // if we reach the last position without opening a file
                        if (m_NextListPosition == m_List.size())
                        {
                            nextMediaObject = nullptr;

                            log_w("Unexpected end of list");
                        }
                        else
                        {
                            // check for a specific start postion
                            if ((m_FileStartPosition) && (m_Resume) && (m_FileStartPosition != nextMediaObject->position()) && (m_FileStartPosition < nextMediaObject->size()))
                            {
                                nextMediaObject->seek(m_FileStartPosition);                               
                            }

                            m_FileStartPosition = 0;

                            m_NextListPosition++;

                            // if we successfully created an object, remember it, so we could manipulate it later
                            m_ActualFile = nextMediaObject;
                        }
                    }
                    else
                    {
                        log_e("Failed to create MediaObject");
                    }
                }
                else
                {
                    log_e("File length is %u", (m_List.at(m_NextListPosition).length()));
                }
            }
            else
            {
                if (m_Verbose)
                {
                    log_d("List finished");
                }
            }
        }
    }
    else
    {
        log_e("List is empty");
    }

    return nextMediaObject;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::loadList(void)
{
    bool result = false;    

    // check again, if the file is what we expect
    if ((m_ObjectPath.length() > 4) && (m_ObjectPath[0] == '/') && (m_ObjectPath.substring(m_ObjectPath.length() - 4).equalsIgnoreCase(".m3u")))
    {
        // make sure the file exists
        if (m_fs->exists(m_ObjectPath))
        {
            File playlist = m_fs->open(m_ObjectPath, "r");

            if (playlist)
            {

                // discard any existing vector
                m_List.clear();

                String currentDirectory;
                String line;
                int ch;

                // get the directory of the current playlist
                currentDirectory = m_ObjectPath.substring(0, m_ObjectPath.lastIndexOf('/') + 1);

                do
                {
                    // read one character
                    ch = playlist.read();

                    // ignore chariage return
                    if ('\r' == ch)
                    {
                    }
                    // handle line feed and file end
                    else if (('\n' == ch) || (EOF == ch))
                    {
                        // if the line is not a comment and not empty
                        if ((line.length()) && (line[0] != '#'))
                        {

                            // replace \ to be fs compatible
                            line.replace('\\', '/');

                            // check for relative or absolute path
                            if (line[0] != '/')
                            {

                                line = currentDirectory + line;

                                if (m_Verbose)
                                {
                                    log_d("Add File \"%s\"", line.c_str());
                                }
                            }
                            else
                            {
                                if (m_Verbose)
                                {
                                    log_d("Add File \"%s\"", line.c_str());
                                }
                            }

                            // make sure we save only mp3 files
                            if ((line.length() > 4) && (line.substring(line.length() - 4).equalsIgnoreCase(".mp3")))
                            {

                                if (m_fs->exists(line))
                                {
                                    // add to the list
                                    m_List.push_back(line);
                                }
                                else
                                {
                                    log_w("File not found \"%s\"", line.c_str());
                                }
                            }
                            else
                            {
                                log_e("Only .mp3 files are supported \"%s\"", line.c_str());
                            }
                        }
                        // clear the line to read the next one from the card
                        line = "";
                    }
                    else
                    {
                        line += (char)ch;
                    }
                } while (EOF != ch);

                if (m_Verbose)
                {
                    log_d("Reading complete \"%s\"", playlist.name());
                }

                // we are done with reading the playlist
                playlist.close();

                // if we read at least one entry, tis is a valid object ;-)
                if (m_List.size())
                {
                    // 
                    if (m_Random)
                    {            
                        std::random_shuffle(m_List.begin(), m_List.end());
                    }

                    result = true;
                }
            }
            else
            {
                log_w("Could not open playlist \"%s\"", m_ObjectPath.c_str());
            }
        }
        else
        {
            log_e("File not found! \"%s\"", m_ObjectPath.c_str());
        }
    }
    else
    {
        log_e("This class could only handle .m3u files (given: \"%s\"", m_ObjectPath.c_str());
    }

    return result;
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectList::getTracksRemaining(void)
{
    uint32_t result = 0;

    result = m_List.size() - m_NextListPosition;

    // if the list is finished and the repeat flag set
    if ((!result) && (m_Repeat))
    {
        result = m_List.size();
    }

    return result;
}

//---------------------------------------------------------------------------------------
void PlayObjectList::Reset(ResetType type)
{
    if (type == ResetType::RESET_FILE)
    {
        if (m_ActualFile)
        {
            m_ActualFile->seek(0);
        }

        m_FileStartPosition = 0;
    }
    else if (type == ResetType::RESET_LIST)
    {
        m_NextListPosition = 0;
        m_FileStartPosition = 0;

        // 
        if (m_Random)
        {
            std::random_shuffle(m_List.begin(), m_List.end());
        }

        if (m_ActualFile)
        {
            // this will cause the getMediaFile method to load the "next" file from the list
            m_ActualFile->seek(m_ActualFile->size());
        }
    }
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectList::getTrackProgress( void )
{   
    uint32_t result = 0;

    if (m_ActualFile)
    {
        result = ((100 * m_ActualFile->position()) / (m_ActualFile->size()));
    }

    return result;
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectList::getPlayListProgress( void )
{
    uint32_t result = 0;

    if ((m_List.size()) && (m_NextListPosition))
    {
        result = (100 * m_NextListPosition / m_List.size());
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::getRandomOrder(void)
{
    return m_Random;
}

//---------------------------------------------------------------------------------------
void PlayObjectList::setRandomOrder(bool random)
{
    m_Random = random;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::getRepeat(void)
{
    return m_Repeat;
}

//---------------------------------------------------------------------------------------
void PlayObjectList::setRepeat(bool repeat)
{
    m_Repeat = repeat;
}

//---------------------------------------------------------------------------------------
bool PlayObjectList::getResume(void)
{
    return m_Resume;
}

//---------------------------------------------------------------------------------------
void PlayObjectList::setResume(bool resume)
{
    m_Resume = resume;
}