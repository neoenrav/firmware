#include "PlayObjectSingle.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "PlayObjSingle";
#endif


//---------------------------------------------------------------------------------------
PlayObjectSingle::PlayObjectSingle()
{     
}


//---------------------------------------------------------------------------------------
PlayObjectSingle::~PlayObjectSingle()
{    
}


//---------------------------------------------------------------------------------------
bool PlayObjectSingle::initialize(const String& jsonFilename, const JsonObject& jsonObj)
{
    bool result = false;

    // restore the
    m_ObjectPath    = jsonObj["Path"] | "";

    m_Resume        = (jsonObj["Resume"] == "TRUE")?true:false;
    m_Repeat        = (jsonObj["Repeat"] == "TRUE")?true:false;

    m_ListPosition  = 0; // a single file has no list :)
    m_FilePosition  = jsonObj["File Position"] | 0; // if resume is not set, we will handle this when the music file is requested

    if (m_ObjectPath.length())
    {
        // remember the filename 
        CardObject::setJsonFileName(jsonFilename);

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
bool PlayObjectSingle::create(const String& filepath, bool repeat, bool resume, String jsonPath)
{
    bool result = false;

    if ((filepath.length()) && (filepath[0] == '/') && (filepath.substring(filepath.length()-4).equalsIgnoreCase(".mp3")))
    {
        if (jsonPath.length())
        {
            // check some path basics
            if ((jsonPath[0] == '/') && (jsonPath.substring(jsonPath.length()-4).equalsIgnoreCase(".json")))
            {
                CardObject::setJsonFileName(jsonPath);
            }
            else
            {
                goto Finish;
            }
        } 
        else
        {
            CardObject::setJsonFileName(filepath + ".json");
        }

        m_ObjectPath    = filepath;

        m_Resume        = resume;
        m_Repeat        = repeat;

        // start always at the beginning
        m_FilePosition  = 0;
        m_ListPosition  = 0;

        result = true;
    }

Finish:
    return result;
}


//---------------------------------------------------------------------------------------
bool PlayObjectSingle::save(const JsonObject& jsonObj)
{
    bool result = false;    

    if  (jsonObj)
    {
        // if we have a file that has not already ended
        if ((m_ActualFile) && (m_ActualFile->position() < m_ActualFile->size()))
        {
            jsonObj["File Position"]    = m_ActualFile->position();
        }
        else
        {
            jsonObj["File Position"]    = 0;
        }

        jsonObj["Resume"]           = m_Resume?"TRUE":"FALSE";
        jsonObj["Repeat"]           = m_Repeat?"TRUE":"FALSE";

        jsonObj["Path"]             = m_ObjectPath;

        result =true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<mediafile::MediaFile> PlayObjectSingle::getMediaFile()
{
    std::shared_ptr<MP3File> nextFileObject = nullptr;

    //check if the file is already finished
    if ((m_ActualFile) && (m_ActualFile->position() < m_ActualFile->size()))
    {
        nextFileObject = m_ActualFile;
    }
    else
    {
        // make sure we do not play more files than available
        if ((m_ListPosition < 1) || (m_Repeat))
        {
            m_ListPosition++;

            // if a valid path and file is available create a single MP3 object and return it
            if (m_ObjectPath.length())
            {
                nextFileObject = std::make_shared<MP3File>();

                // if we successfully created an object, remember it, so we could manipulate it later
                if (nextFileObject)
                {
                    int32_t result;

                    result = nextFileObject->open(m_ObjectPath);
                    if (result)
                    {
                        // check the resume flag and seek for the right position
                        if ((m_ListPosition <= 1) && (m_Resume) && (m_FilePosition < nextFileObject->position()))
                        {
                            nextFileObject->seek(m_FilePosition);
                        }
                        else
                        {
                            m_FilePosition = 0;
                        }

                        m_ActualFile = nextFileObject;
                    }
                    else
                    {
                        log_e("Failed to open file \"%s\" result %ld", m_ObjectPath.c_str(), result);
                    }
                }
            }
        }
    }

    return nextFileObject;
}


//---------------------------------------------------------------------------------------
uint32_t PlayObjectSingle::getTracksRemaining( void )
{
    uint32_t result = 0;

    if (m_Repeat)
    {
        result = 1;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void PlayObjectSingle::Reset( ResetType type )
{    
    if ( type == ResetType::RESET_FILE)
    {
        if (m_ActualFile)
        {
            m_FilePosition = 0;
            m_ActualFile->seek(0);
        }

        m_ListPosition = 0;
    }
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectSingle::getTrackProgress( void )
{
    uint32_t result = 0;

    if (m_ActualFile)
    {
        result = ((100 * m_ActualFile->position()) / (m_ActualFile->size()));
    }

    return result;
}

//---------------------------------------------------------------------------------------
uint32_t PlayObjectSingle::getPlayListProgress( void )
{
    uint32_t result = 0;

    if (m_ActualFile)
    {
        if (m_ActualFile->position() == m_ActualFile->size())
        {
            result = 100;
        }
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool PlayObjectSingle::getRandomOrder( void )
{
    return false;
}

//---------------------------------------------------------------------------------------
void PlayObjectSingle::setRandomOrder( bool random )
{
    
}

 //---------------------------------------------------------------------------------------
bool PlayObjectSingle::getRepeat( void )
{
    return m_Repeat;   
}

//---------------------------------------------------------------------------------------
void PlayObjectSingle::setRepeat( bool repeat )
{
    m_Repeat = repeat;
}

//---------------------------------------------------------------------------------------
bool PlayObjectSingle::getResume( void )
{
    return m_Resume;
}

//---------------------------------------------------------------------------------------
void PlayObjectSingle::setResume( bool resume )
{
    m_Resume = resume;
}