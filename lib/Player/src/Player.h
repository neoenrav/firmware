/** @file Player.h
 * 
 * @brief Main thread for the NeoEnRav music box. Handels
 * all events and controls the outputs.
 * 
 *
 * 
 * Licensed under GNU GPLv3 <http://gplv3.fsf.org/>
 * Copyright © 2017
 *
 * @authors enrav, asealion,
 *
 * Development log:
 *  - 2019: first version with MRCF522 binding
 *          by Mike Reinecke (github: @asealion )
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License or later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef LIB_PLAYER_SRC_PLAYER_H_
    #define LIB_PLAYER_SRC_PLAYER_H_

    #include <Preferences.h>
    #include <queue>
    #include <memory>

    #include "Arduino.h"

    #include "thread.hpp"
    #include "mutex.hpp"
    #include "queue.hpp"

    #include "PlayObject.h"

    #include "VS1053.h"
    #include "RFID.h"
    #include "StatusLed.h"
    #include "Buttons.h"

    /*! \brief Brief description.
        *         Brief description continued.
        *
        *  Detailed description starts here.
        */
    class Player: public cpp_freertos::Thread
    {
        public:
            enum class PlayerStatus
            {
                PLAYER_IDLE,
                PLAYER_ACTIVE
            };

            bool        m_Verbose;
            
        public:
            Player(/* args */);
            ~Player();

            /**  @brief Use this functions to connect the different classes (RFID, decoder etc.) to the player
             * 
             * */
            void        Attach( vs1053::VS1053 *pVS1053 ) ; 
            void        Attach( RFID *pRfReader );
            void        Attach( StatusLed *pStatusLed);
            void        Attach( Buttons *pButtons);

            // try to start the thread ( only possible if it is configured/connected )
            bool        Start( void );   

            //----------------------------------------
            // This are the functions to controle the player. They are only available after starting the thread
            //----------------------------------------

            /** @brief Plays a single "file" (MP3-file, Playlist, directory with MP3s or .json)
             * 
             *  To play a single file, we create a new PlaySingleFile object ans send it to the 
             *  player stack.
             * */
            bool        PlayFile(String path, bool resume = false, bool repeat = false, bool shuffle = false);

            /** @brief Pause an active playback or resume a paused file
             * 
             * */
            bool        PlayerPauseResume( void );            

            /** @brief Restart the active object from the beginning of the first file
             * 
             *  @param completeObject - true, the actual play object with be reset completely
             *                          false, rewinds only the active MP3 file
             * */
            bool        PlayerReset( bool completeObject = false );


            /** @brief 
             * 
             * */
            void        setStopOnCardRemove(bool value);
            bool        getStopOnCardRemove( void );

            /** @brief Get the actual volume [1..(max)]
             * */
            uint32_t    getVolume(void);

            /** @brief Set the actual volume [1..(max)]
             * */
            bool        setVolume(uint32_t value);

            /** @brief Get the maximum allowed volume [1..10]
             * */
            uint32_t    getMaximumVolume(void);

            /** @brief Set the maximum allowed volume [1..10]
             *  The the maximum allowed volume. 
             *  If the actual or default value is above this limit it will automatically 
             *  be reduced. 
             * */
            bool        setMaximumVolume(uint32_t value);

            /** @brief Get the default volume that is used after reset [1..(max)]
             * */
            uint32_t    getDefaultVolume(void);

            /** @brief Set the default volume used after reset [1..(max)]
             * */
            bool        setDefaultVolume(uint32_t value);



        protected:
        
            void        Run( void );                                // the "real" thread function

        private:            

            // handle incoming objects 
            cpp_freertos::MutexStandard                 PlayObjectQueueMutex;   
            std::queue<std::shared_ptr<PlayObject>>     PlayObjectQueue;

            // our own configuration
            bool                                        Configured;
            vs1053::VS1053                              *m_pVS1053;
            RFID                                        *m_pReader;
            StatusLed                                   *m_pStatusLed;
            Buttons                                     *m_pButtons;

            // dynamic data
            std::shared_ptr<PlayObject>                 m_ActualPlayObject;
            
            PlayerStatus                                m_Status;

            uint32_t                                    m_Volume;

            Preferences                                 m_Preferences;
    
            struct Settings
            {
                bool                                        StopOnCardRemove;
                uint32_t                                    DefaultVolume;
                uint32_t                                    MaximumVolume;
            }                                           m_Settings;
            

        private:
                        
            /** @brief Load settings from NVS
             * 
             * */
            void        loadSettings(void);

    };
    
#endif // LIB_PLAYER_SRC_PLAYER_H_
        