#include "Player.h"

#include <memory>
#include <vector>

#include "CardHandler.h"

#include "CardObject.h"
#include "PlayObject.h"
#include "PlayObjectSingle.h"


#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "Player";
#endif

// Notifications
static const uint32_t NOTIFICATION__NEW_PLAY_OBJECT         = 0x00000001;

static const uint32_t NOTIFICATION__PAUSE_OBJECT            = 0x00000010;
static const uint32_t NOTIFICATION__RESUME_OBJECT           = 0x00000020;

static const uint32_t NOTIFICATION__PLAY_NEXT_SONG          = 0x00000100;

static const uint32_t NOTIFICATION__RESET_SONG              = 0x00001000;
static const uint32_t NOTIFICATION__RESET_OBJECT            = 0x00002000;

static const uint32_t NOTIFICATION__NEW_CARD                = 0x00010000;
static const uint32_t NOTIFICATION__CARD_REMOVED            = 0x00020000;

static const uint32_t NOTIFICATION__INCREMENT_VOLUME        = 0x00100000;
static const uint32_t NOTIFICATION__DECREMENT_VOLUME        = 0x00200000;
static const uint32_t NOTIFICATION__UPDATE_VOLUME           = 0x00300000;

static const uint32_t NOTIFICATION__DECODING_STOPPED        = 0x01000000;

//---------------------------------------------------------------------------------------
Player::Player(/* args */):
    Thread("Player", 12288, tskIDLE_PRIORITY),    
    m_Verbose(true),    
    m_pVS1053(NULL),
    m_pReader(NULL),
    m_pStatusLed(NULL),
    m_pButtons(NULL),
    m_ActualPlayObject(nullptr),
    m_Status(PlayerStatus::PLAYER_IDLE),
    m_Preferences()
{
}


//---------------------------------------------------------------------------------------
Player::~Player()
{
}

//---------------------------------------------------------------------------------------
void Player::Attach( vs1053::VS1053 *pDecoder )
{
    if ((NULL == this->m_pVS1053) && (NULL != pDecoder))
    {
        this->m_pVS1053 = pDecoder;

        if (m_Verbose)
        {
            ESP_LOGD(TAG, "Attached pointer to decoder");
        }
    }
}


//---------------------------------------------------------------------------------------
void Player::Attach( RFID *pRfReader )
{
    if ((NULL == this->m_pReader) && (NULL != pRfReader))
    {
        this->m_pReader = pRfReader;

        if (m_Verbose)
        {
            ESP_LOGD(TAG, "Attached pointer to RF reader");
        }
    }
}

//---------------------------------------------------------------------------------------
void Player::Attach( StatusLed *pStatusLed )
{
    if ((NULL == this->m_pStatusLed) && (NULL != pStatusLed))
    {
        this->m_pStatusLed = pStatusLed;

        if (m_Verbose)
        {
            ESP_LOGD(TAG, "Attached pointer to Status LED");
        }
    }
}

//---------------------------------------------------------------------------------------
void Player::Attach( Buttons *pButtons )
{
    if ((NULL == this->m_pButtons) && (NULL != pButtons))
    {
        this->m_pButtons = pButtons;

        if (m_Verbose)
        {
            ESP_LOGD(TAG, "Attached pointer to Buttons");
        }
    }
}

//---------------------------------------------------------------------------------------
bool Player::Start( void )
{
    bool result = false;

    if (this->m_pVS1053)
    {
        result = Thread::Start();

        // make sure the other task tell us, if you have some information for us
        if (result)
        {
            // if the decoder has finished a track
            if (m_pVS1053)
            {
                m_pVS1053->RegisterFileCompleteNotification(this->GetHandle(), NOTIFICATION__DECODING_STOPPED);
            }

            // the card handler has a new card / or the active one is gone
            if (m_pReader)
            {
                m_pReader->registerCardDetected(this->GetHandle(), NOTIFICATION__NEW_CARD);
                m_pReader->registerCardRemoved(this->GetHandle(), NOTIFICATION__CARD_REMOVED);
            }

            if (m_pButtons)
            {
                m_pButtons->registerButtonEvent(Buttons::ButtonEvents::PAUSE, this->GetHandle(), (NOTIFICATION__PAUSE_OBJECT | NOTIFICATION__RESUME_OBJECT));
                m_pButtons->registerButtonEvent(Buttons::ButtonEvents::RESET, this->GetHandle(), NOTIFICATION__RESET_OBJECT);

                m_pButtons->registerButtonEvent(Buttons::ButtonEvents::VOLUME_UP,   this->GetHandle(), NOTIFICATION__INCREMENT_VOLUME);
                m_pButtons->registerButtonEvent(Buttons::ButtonEvents::VOLUME_DOWN, this->GetHandle(), NOTIFICATION__DECREMENT_VOLUME);
            }
        }
    }

    return result;
}


//---------------------------------------------------------------------------------------
void Player::Run( void )
{   
    uint32_t ulNotifiedValue;

    if (m_pStatusLed)
    {
        m_pStatusLed->setPattern(StatusLed::Pattern::BUSY);
    }

    DelayMS(5000);

    loadSettings();

    // show the idle pattern
    if (m_pStatusLed)
    {
        m_pStatusLed->setPattern(StatusLed::Pattern::IDLE);
    }

    // set the start volume
    setVolume(m_Settings.DefaultVolume);


    while (1)
    {
        // check for notification (Don't clear bits on entry, Clear all bits on exit, if nothing happens, we will create an update every 2000ms )
        xTaskNotifyWait( pdFALSE,  ULONG_MAX, &ulNotifiedValue, 2000 );

        // update general data (progress, ... )
        if (m_ActualPlayObject)
        {
            if (m_Status == PlayerStatus::PLAYER_ACTIVE)
            {
                uint32_t progress = m_ActualPlayObject->getTrackProgress();

                if (m_pStatusLed)
                {
                    m_pStatusLed->setPattern(StatusLed::Pattern::TRACK_PROGRESS, progress);
                }
            }
        }

        // if we have a new card, try to get a card object
        if (ulNotifiedValue & NOTIFICATION__NEW_CARD)
        {
            std::shared_ptr<RFCard> newRfCard = m_pReader->getCard();

            std::vector<std::shared_ptr<CardObject>> newCardObject = CardHandler::getCardObject(newRfCard.get());

            if (newCardObject.size()) 
            {
                bool playObjectAdded = false;

                for ( uint32_t counter = 0; counter < newCardObject.size(); counter++)
                {

                    if (newCardObject[counter]->GetCardType() == CardObject::CardType::PLAY)
                    {
                        if (playObjectAdded == false)
                        {
                            // make sure no one access the stack when we add an object
                            PlayObjectQueueMutex.Lock();

                            // since the type is play and all play objects are card, we could cast the result
                            PlayObjectQueue.push(std::static_pointer_cast<PlayObject>(newCardObject[counter]));

                            PlayObjectQueueMutex.Unlock();

                            // extend the active flags to start the playback
                            ulNotifiedValue |= NOTIFICATION__NEW_PLAY_OBJECT;

                            playObjectAdded = true;
                        }
                        else
                        {
                            log_w("Multiple Play-Objects found.");
                        }
                    }
                    else if (newCardObject[counter]->GetCardType() == CardObject::CardType::MODIFICATOR)
                    {
                        // show the error event pattern
                        if (m_pStatusLed)
                        {
                            m_pStatusLed->setPattern(StatusLed::Pattern::ERROR);
                        }

                        ESP_LOGW(TAG, "Card Type not yet implemented");
                    }
                    else 
                    {

                        // show the error event pattern
                        if (m_pStatusLed)
                        {
                            m_pStatusLed->setPattern(StatusLed::Pattern::ERROR);
                        }

                        ESP_LOGE(TAG, "Card Type not supported \"%s\"", newCardObject[counter]->GetCardTypeString().c_str());
                    }
                }
            }
            else
            {
                // show the error event pattern
                if (m_pStatusLed)
                {
                    m_pStatusLed->setPattern(StatusLed::Pattern::ERROR);
                }

                ESP_LOGE(TAG, "RF Card \"%s\"not found ", newRfCard->toString().c_str());
            }
        }

        // check if we got a new object to play, if this is true, all other tasks will be discarded
        if (ulNotifiedValue & NOTIFICATION__NEW_PLAY_OBJECT)
        {
             // get the new object
            PlayObjectQueueMutex.Lock();

            // if there are objects in the queue 
            if (!PlayObjectQueue.empty())
            {
                m_ActualPlayObject = PlayObjectQueue.front();
                PlayObjectQueue.pop();                
            }

            PlayObjectQueueMutex.Unlock();

            std::shared_ptr<mediafile::MediaFile> nextSong = m_ActualPlayObject->getMediaFile();

            if (m_pVS1053->PlaySong(nextSong))
            {
                if (m_pStatusLed)
                {
                    m_pStatusLed->setPattern(StatusLed::Pattern::PLAYLIST_PROGRESS, m_ActualPlayObject->getPlayListProgress());
                }

                if (m_Verbose)
                {
                    log_d("Start File \"%s\"", nextSong->getTitle());
                }
            }
            else
            {
                log_e("Start of song failed");
            }

            m_Status = PlayerStatus::PLAYER_ACTIVE;

            //clear all other notifications
            ulNotifiedValue = 0;
        }

        //if the decoder has finished the actual file, check if the actual object has more to "do"
        if (ulNotifiedValue & NOTIFICATION__DECODING_STOPPED)
        {
            // If we are active and the decoding stop's this means we could continue with the next track
            if (m_Status == PlayerStatus::PLAYER_ACTIVE)
            {
                // check if the play object has more tracks
                if (m_ActualPlayObject->getTracksRemaining())
                {
                    std::shared_ptr<mediafile::MediaFile> nextFile = m_ActualPlayObject->getMediaFile();

                    if (nextFile)
                    {
                        if (m_pVS1053->PlaySong(nextFile))
                        {
                            if (m_pStatusLed)
                            {
                                m_pStatusLed->setPattern(StatusLed::Pattern::PLAYLIST_PROGRESS, m_ActualPlayObject->getPlayListProgress());
                            }

                            if (m_Verbose)
                            {
                                ESP_LOGD(TAG, "Starting song \"%s\"", nextFile->getTitle());
                            }
                        }
                        else
                        {
                            if (m_pStatusLed)
                            {
                                m_pStatusLed->setPattern(StatusLed::Pattern::ERROR);
                            }

                            log_w("Start of song \"%s\" failed", nextFile->getTitle());
                        }
                    }
                    else
                    {
                        if (m_pStatusLed)
                        {
                            m_pStatusLed->setPattern(StatusLed::Pattern::ERROR);
                        }

                        log_e("Failed to get next track from object!");
                    }
                }
                else
                {
                    // if a list is finished, make sure it is started from the beginning next time
                    m_ActualPlayObject->Reset(PlayObject::ResetType::RESET_LIST);

                    CardHandler::updateCardObject(m_ActualPlayObject);

                    m_Status = PlayerStatus::PLAYER_IDLE;

                    // show the idle pattern
                    if (m_pStatusLed)
                    {
                        m_pStatusLed->setPattern(StatusLed::Pattern::IDLE);
                    }

                    ESP_LOGI(TAG, "Play Object complete");
                }
            }
        }

        // if the card was removed from the reader
        if (ulNotifiedValue & NOTIFICATION__CARD_REMOVED)
        {
            if (m_Settings.StopOnCardRemove)
            {
                if (m_Status == PlayerStatus::PLAYER_ACTIVE)
                {
                    ulNotifiedValue |= NOTIFICATION__PAUSE_OBJECT;
                }
            }
        }

        // rewind the complete playlist
        if (ulNotifiedValue & NOTIFICATION__RESET_OBJECT)
        {
            // if the player is active, stop it
            if (m_Status == PlayerStatus::PLAYER_ACTIVE)
            {
                ulNotifiedValue |= NOTIFICATION__PAUSE_OBJECT;

                xTaskNotify(this->GetHandle(), (NOTIFICATION__RESET_OBJECT | NOTIFICATION__RESUME_OBJECT), eSetBits);
            }
            else
            {
                if (m_ActualPlayObject)
                {
                    // reset the track
                    m_ActualPlayObject->Reset(PlayObject::ResetType::RESET_LIST);

                    if (m_Verbose)
                    {
                        log_d("reset object");
                    }
                }
            }
        }

        // restart the actual file
        if (ulNotifiedValue & NOTIFICATION__RESET_SONG)
        {
            // if the player is active, stop it
            if (m_Status == PlayerStatus::PLAYER_ACTIVE)
            {
                ulNotifiedValue |= NOTIFICATION__PAUSE_OBJECT;

                xTaskNotify(this->GetHandle(), (NOTIFICATION__RESET_SONG | NOTIFICATION__RESUME_OBJECT), eSetBits);
            }
            else
            {
                if (m_ActualPlayObject)
                {
                    // reset the track
                    m_ActualPlayObject->Reset(PlayObject::ResetType::RESET_FILE);

                    if (m_Verbose)
                    {
                        log_d("reset track");
                    }
                }
            }
        }

        // pause or resume the actual object
        if (ulNotifiedValue & (NOTIFICATION__PAUSE_OBJECT | NOTIFICATION__RESUME_OBJECT))
        {
            if (m_Status == PlayerStatus::PLAYER_ACTIVE)
            {
                m_pVS1053->StopSong();

                m_Status = PlayerStatus::PLAYER_IDLE;

                CardHandler::updateCardObject(m_ActualPlayObject);

                if (m_ActualPlayObject) 
                {
                    uint32_t progress = m_ActualPlayObject->getTrackProgress();

                    if (m_pStatusLed)
                    {
                        m_pStatusLed->setPattern(StatusLed::Pattern::PAUSE, progress);
                    }
                }

                if (m_Verbose)
                {
                    ESP_LOGD(TAG, "pause track");
                }
            }
            else
            {
                if (m_ActualPlayObject)
                {
                    std::shared_ptr<mediafile::MediaFile> mediaFile = m_ActualPlayObject->getMediaFile();

                    if (mediaFile)
                    {
                        if (m_pVS1053->PlaySong(mediaFile));
                        {
                            m_Status = PlayerStatus::PLAYER_ACTIVE;

                            if (m_Verbose)
                            {
                                ESP_LOGD(TAG, "Continue track");
                            }
                        }
                    }
                }
                else
                {
                    log_i("No active object to pause");
                }
                
            }
        }

        // increment the volume one step
        if (ulNotifiedValue & (NOTIFICATION__INCREMENT_VOLUME))
        {
            if (m_Volume < m_Settings.MaximumVolume)
            {
                m_Volume++;
                ulNotifiedValue |= NOTIFICATION__UPDATE_VOLUME;
            }
        }

        // decrement the volume one step
        if (ulNotifiedValue & (NOTIFICATION__DECREMENT_VOLUME))
        {
            if (m_Volume > 1)
            {
                m_Volume--;
                ulNotifiedValue |= NOTIFICATION__UPDATE_VOLUME;
            }
        }

        // send the actual volume to the decoder / amplifier
        if (ulNotifiedValue & (NOTIFICATION__UPDATE_VOLUME))
        {
            // send the new value
            m_pVS1053->SetVolume(m_Volume * 2);

            // update the led 
            if (m_pStatusLed)
            {
                m_pStatusLed->setPattern(StatusLed::Pattern::VOLUME, m_Volume * 10);
            }

            log_d("Set volume to %u", m_Volume);
        }
    }
}


//---------------------------------------------------------------------------------------
bool Player::PlayerPauseResume( void ) 
{
    bool result = false;

    if ((m_pVS1053 && m_ActualPlayObject))
    {
        //Set Task notification flag
        xTaskNotify(this->GetHandle(), (NOTIFICATION__PAUSE_OBJECT | NOTIFICATION__RESUME_OBJECT), eSetBits);

        result = true;
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool Player::PlayerReset( bool completeObject )
{
    bool result = false;

    if ((m_pVS1053 && m_ActualPlayObject))
    {
        //Set Task notification flag
        if (completeObject)
        {
            xTaskNotify(this->GetHandle(), NOTIFICATION__RESET_OBJECT, eSetBits);
        }
        else
        {
            xTaskNotify(this->GetHandle(), NOTIFICATION__RESET_SONG, eSetBits);
        }

        result = true;
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool Player::PlayFile(String path, bool resume, bool repeat, bool shuffle)
{
    bool result = false;

    // try to get a card object for this filename
    std::shared_ptr<CardObject> newCardObject = CardHandler::getCardObject(path);

    if (newCardObject)
    {
        // we could only continue, if the object is a play type
        if (newCardObject->GetCardType() == CardObject::CardType::PLAY)
        {

            // check for additional flags

            // resume
            if (resume)
            {
                std::static_pointer_cast<PlayObject>(newCardObject)->setResume(true);
            }

            // make sure no one access the stack when we add an object
            PlayObjectQueueMutex.Lock();

            // since the type is play and all play objects are card, we could cast the result
            PlayObjectQueue.push(std::static_pointer_cast<PlayObject>(newCardObject));

            PlayObjectQueueMutex.Unlock();

            // tell the player to look after the play object queue
            xTaskNotify(this->GetHandle(), NOTIFICATION__NEW_PLAY_OBJECT, eSetBits);

            if (m_Verbose)
            {
                ESP_LOGD(TAG, "Added PlayObject to queue");
            }

            result = true;

        }
        else
        {
            ESP_LOGI(TAG, "Could only play \"PLAY\" cards (Modificator cards are not possible to play)");
        }
    }
    else
    {
        ESP_LOGW(TAG, "No CardObject available for \"%s\"", path.c_str());
    }

    return result;
}

//---------------------------------------------------------------------------------------
void Player::loadSettings(void)
{
    const uint32_t ActualVersion = 2;

    m_Preferences.begin("Player", false);

    uint32_t version = m_Preferences.getULong("Version", 0);

    
    // load all settings (and default values)
    m_Settings.StopOnCardRemove  = m_Preferences.getBool("Stop", true);
    m_Settings.MaximumVolume     = m_Preferences.getULong("MaxVolume", 10);
    m_Settings.DefaultVolume     = m_Preferences.getULong("Volume", 8);

    // check the version number and update new settings
    switch (version)
    {
        case 0:
            m_Preferences.putBool("Stop", m_Settings.StopOnCardRemove);
        
        case 1:
            m_Preferences.putULong("MaxVolume", m_Settings.MaximumVolume);
            m_Preferences.putULong("Volume",    m_Settings.DefaultVolume);

        //----------------
            log_d("Updated Player-Settings to v%u", ActualVersion);

            m_Preferences.putULong("Version", ActualVersion);

        case ActualVersion:
            // no need to update
            break;
        
        default:
            break;
    }
}

//---------------------------------------------------------------------------------------
void Player::setStopOnCardRemove(bool value)
{
    if (m_Settings.StopOnCardRemove != value)
    {
        m_Settings.StopOnCardRemove = value;
        m_Preferences.putBool("Stop", value);
    }
}

//---------------------------------------------------------------------------------------
bool Player::getStopOnCardRemove( void )
{
    return m_Settings.StopOnCardRemove;
}

//---------------------------------------------------------------------------------------
uint32_t Player::getVolume(void)
{
    return m_Volume;
}

//---------------------------------------------------------------------------------------
bool Player::setVolume(uint32_t value)
{
    bool result = false;

    if ((value > 0) && (value <= m_Settings.MaximumVolume))
    {
        if (m_Volume != value)
        {
            m_Volume = value;
            
            xTaskNotify(this->GetHandle(), NOTIFICATION__UPDATE_VOLUME, eSetBits);
        }
        result = true;
    }

    return result;
}

//---------------------------------------------------------------------------------------
uint32_t Player::getMaximumVolume(void)
{
    return m_Settings.MaximumVolume;
}

//---------------------------------------------------------------------------------------
bool Player::setMaximumVolume(uint32_t value)
{
    bool result = false;

    if ((value > 0) && (value <= 10))
    {
        if (getVolume() > value)
        {
            setVolume(value);
        }

        if (getDefaultVolume() > value)
        {
            setDefaultVolume(value);
        }

        if (value != m_Settings.MaximumVolume)
        {
            m_Settings.MaximumVolume = value;
            m_Preferences.putULong("MaxVolume", value);
        }
        result = true;
    }
    return result;
}

//---------------------------------------------------------------------------------------
uint32_t Player::getDefaultVolume(void)
{
    return m_Settings.DefaultVolume;
}

//---------------------------------------------------------------------------------------
bool Player::setDefaultVolume(uint32_t value)
{
    bool result = false;

    if ((value > 0) && (value <= m_Settings.MaximumVolume))
    {
        if (m_Settings.DefaultVolume != value)
        {
            m_Settings.DefaultVolume = value;

            m_Preferences.putULong("Volume", m_Settings.DefaultVolume);
        }
        result = true;
    }

    return result;

}