#ifndef PLAYOBJECT_LIST_H
    #define PLAYOBJECT_LIST_H

    #include <memory>
    #include <vector>

    #include "Arduino.h"
    #include "FS.h"
    #include "PlayObject.h"
    #include "Mp3File.h"
    
    /*! \brief This class covers the playback of a single MP3 file.
        *
        *  Detailed description starts here.
        */
    class PlayObjectList : public PlayObject
    {
        public:
            /** @brief Enables the debug log messages of this class
             * 
             * */
            bool    m_Verbose;

        public:
            PlayObjectList();

            ~PlayObjectList();

            // identification from Play Object
            const PlayObject::PlayType getPlayType() { return PlayObject::PlayType::LIST; };
            const String               getPlayTypeString() { return "List"; };


            /** @brief Try to "recreate" an object from a json object
             * 
             * */
            bool initialize(const String& jsonFilename, const JsonObject& jsonObj);

            /** @brief Initialize the card if there is no json file available
             * 
             *  @param filepath - the path to the file 
             *  @param repeat   - false -> stop the playback when the file is finished
             *                  - true -> restart the file when it was finished
             *  @param resume   - remember the last position when the card is initialized
             * */
            bool create(const String& filePath, bool repeat = false, bool resume = false, bool random = false, String jsonPath = "");

            /** @brief Adds all settings of this card to the json object
             * 
             * */
            bool save(const JsonObject& jsonObj);

            /** @brief Get the next media file for this card
             * 
             * */
            std::shared_ptr<mediafile::MediaFile> getMediaFile( void );            

            /** @brief Query the number of media files remaining
             * 
             * */
            uint32_t getTracksRemaining( void );

            /** @brief Reset the playback position of the actual file
             * 
             *  Set the read point to the beginning of the file
             * 
             * */
            void Reset( ResetType type );

            /** @brief Get the % already read from the actual file
             * 
             * @retval [0..100]% from the file position
             * */ 
            uint32_t getTrackProgress( void );

            /** @brief Get how many tracks (in percent) are played
             * 
             * @retval [0..100%] from the tracks in the playlist
             * */
            uint32_t getPlayListProgress( void );


            /** @brief
             * 
             * */
            bool getRandomOrder( void );

            /** @brief
             * 
             * */
            void setRandomOrder( bool random );

            /** @brief
             * 
             * */
            bool getRepeat( void );

            /** @brief
             * 
             * */
            void setRepeat( bool random );

            /** @brief
             * 
             * */
            bool getResume( void );

            /** @brief
             * 
             * */
            void setResume( bool random );

        private:
            fs::FS                      *m_fs;
            String                      m_ObjectPath;
            bool                        m_Resume;
            bool                        m_Repeat;
            bool                        m_Random;

            /** @brief The position of the list that is read when the 
             *          getMediaFile method is called
             * 
             * */
            uint32_t                    m_NextListPosition;

            /** @brief 
             * 
             * */
            uint32_t                    m_FileStartPosition;

            /** @brief
             * 
             * */
            std::shared_ptr<MP3File>    m_ActualFile;

            const uint32_t              m_MaxFilePathLength = 255;
            std::vector<String>         m_List;

        private:            

            /** 
             * @brief Loads the specified playlist file and stores it in a std::vector
             * 
             *  Reads a .m3u file from the given file system and stores all .mp3 files
             *  (#-lines are ignored, other endings would cause an error) in the m_List
             *  std::vector of the play object
             * 
             * @result  true    -> the playlist vector was successfully populated
             *          false   -> error while creating the vector 
             * */
            bool loadList(void);
    };

    
#endif