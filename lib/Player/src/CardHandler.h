#ifndef CARDHANDLER_H
    #define CARDHANDLER_H

    #include <memory>
    
    #include "Arduino.h"
    #include "RFID.h"

    #include <FS.h>

    #include "SD.h"

    #include "CardObject.h"
    // #include "PlayObject.h"
    // #include "PlayObjectSingle.h"

    #include "RFID.h"

    /*! \brief Handle the Card information and creates the
        *         PlayObject that is needed for the Player.
        *
        *  Detailed description starts here.
        */
    class CardHandler
    {

        public:

            /** @brief Get a Card Object for the provided RF Card
             * 
             *  the function will check the internal memory if the provided RF card is linked 
             *  with a .json container file.
             *  If the container exists a card object is created from the data and returned to the caller
             *  ( if the linked json file does not exists ( or the RF card is unknown ), a null_prt is returned)
             * 
             * */
            static std::vector<std::shared_ptr<CardObject>> getCardObject( RFCard *pCard );

            /** @brief Get a Card Object for the provided file name
             * 
             *  If the filename is a .json file the function will try to create an object
             *  from the file and return the result (if the file does not exists, 
             *  a null_prt is returned)
             * 
             *  If the filename is a directory, a playlist (.m3u) or a MP3 file (.mp3)
             *  the function will try to read corresponding filename.json file.
             *  If such a file does not exist, it is created.
             * 
             * */
            static std::shared_ptr<CardObject> getCardObject( String filename );

            /** @brief Create a new (not initialized) card object
             * 
             * */
            static std::shared_ptr<CardObject> createNewCardObject( String objectFileName, String containerFileName = "", CardObject::CardType type = CardObject::CardType::PLAY );

            /** @brief write / update the json stucture of a card object to the SD card
             * 
             * */
            static bool updateCardObject( const std::shared_ptr<CardObject>& CardObject );

            /** @brief Create a new link between the RF card and a card object
             * 
             * */
            static bool linkRfCardToCardObject(RFCard& card, CardObject& cardObject);

            /** @brief Add an additional Card Object to an existing RF card
             * 
             * */
            static bool addCardObjectToRfCard(RFCard& card, CardObject& cardObject);

            /** @brief Delete the link between the RF card and a card object
             *
             *  Deleeting the last link will also remove the json file
             * */
            static bool deleteCardObjectFromRfCard(RFCard& card, CardObject& cardObject);

        private:
            CardHandler(/* args */);

            /* methods */
            static bool loadJsonFile(const String& jsonFile, JsonDocument *jsonDoc);


            static std::shared_ptr<CardObject> initializePlaySingleObject(const String& fileName, JsonObject *obj );

            static std::shared_ptr<CardObject> initializePlayFolderObject(const String& fileName, JsonObject *obj );

            static std::shared_ptr<CardObject> initializePlayListObject(const String& fileName, JsonObject *obj );

        private:
            /* data */

    };
    
#endif
