#ifndef CARDOBJECT_H
    #define CARDOBJECT_H    

    #include "Arduino.h"

    #include "ArduinoJson.h"

    /*! \brief This class covers all different play modes.
        *
        *  Detailed description starts here.
        */
    class CardObject
    {
        public:
            enum class CardType 
            {
                PLAY,
                MODIFICATOR,
                UNKNOWN
            };

        public:
            virtual ~CardObject() {};

            /** This method must be implemented by the "top" class to fill the object with "life"
             * 
             * */
            virtual bool            initialize(const String& jsonFilename, const JsonObject& jsonObj) = 0;

            /**
             * 
             * */
            virtual bool            save(const JsonObject& jsonObj) = 0;

            virtual const CardType  GetCardType( void ) = 0;
            virtual const String    GetCardTypeString( void ) = 0;
            
            virtual String          getJsonFileName( void );

        protected:

            virtual void            setJsonFileName( const String& jsonFileName );

            /// @brief The path we the JSON file for this card a saved
            String                  m_jsonFile;

    };
    
#endif