#include "CardObject.h"

#include "SD.h"
#include "ArduinoJson.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "CardObject";
#endif


//---------------------------------------------------------------------------------------
void CardObject::setJsonFileName( const String& jsonFileName )
{
    m_jsonFile = jsonFileName;
}

//---------------------------------------------------------------------------------------
String CardObject::getJsonFileName( void )
{
    return m_jsonFile;
}
