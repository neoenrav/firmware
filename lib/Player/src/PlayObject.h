#ifndef PLAYOBJECT_H
    #define PLAYOBJECT_H

    #include <memory>

    #include "CardObject.h"
    #include "MediaFile.h"

    #include <ArduinoJson.h>
    
    /*! \brief This class covers all different play modes.
        *
        *  Detailed description starts here.
        * 
        *  Possible modes / objects:
        *   - single file
        *   - single file (never ending)
        *   - playlist file
        *   - playlist file (random)
        *   - playlist file (never ending)
        *   - playlist file (random, never ending)
        *   - all files from a directory (sorted by "filesystem")
        *   - all files from a directory (random)
        *   - all files from a directory (sorted by "filesystem", never ending)
        *   - all files from a directory (random, never ending)
        *   - audiobook (save playlist and file position on pause / sleep)
        *   - audiobook (save playlist and file position on pause / sleep, never ending)
        */
    class PlayObject: public CardObject
    {
        public:
        enum class ResetType
        {
            RESET_FILE,
            RESET_LIST
        };


        enum class PlayType
        {
            SINGLE_FILE,
            FOLDER,
            LIST,
            BOOK
        };

        public:
            virtual ~PlayObject() {};

            // from Card Object
            const CardType GetCardType() { return m_CardType; };
            const String   GetCardTypeString() { return "Play"; };            

            //
            virtual const PlayObject::PlayType getPlayType();
            virtual const String               getPlayTypeString();


            /** @brief Get the a file object (MP3) to play
             *  
             *  If the object has a resume function the MP3 will start at the 
             *  position is was saved. 
             *  The position will be resetted, if the file reaches its end
             * */
            virtual std::shared_ptr<mediafile::MediaFile> getMediaFile( void ) = 0;
            

            /** @briefQuery the number of media files remaining
             * 
             * */
            virtual uint32_t getTracksRemaining( void ) = 0;


            /** @brief Reset the playback position of the actual file
             * 
             *  Set the read point to the beginning of the file
             * 
             * */
            virtual void Reset( ResetType type ) = 0;

            /** @brief Get the % already read from the actual file
             * 
             * @retval [0..100]% from the file position
             * */ 
            virtual uint32_t getTrackProgress( void ) = 0;

            /** @brief Get how many tracks (in percent) are played
             * 
             * @retval [0..100%] from the tracks in the playlist
             * */
            virtual uint32_t getPlayListProgress( void ) = 0; 

            /** @brief
             * 
             * */
            virtual bool getRandomOrder( void ) = 0 ;

            /** @brief
             * 
             * */
            virtual void setRandomOrder( bool random ) = 0 ;

            /** @brief
             * 
             * */
            virtual bool getRepeat( void ) = 0 ;

            /** @brief
             * 
             * */
            virtual void setRepeat( bool random ) = 0 ;


            /** @brief
             * 
             * */
            virtual bool getResume( void ) = 0 ;

            /** @brief
             * 
             * */
            virtual void setResume( bool random ) = 0 ;

        protected:
            

        private:            
            const CardType  m_CardType = CardType::PLAY;

    };
    
#endif
