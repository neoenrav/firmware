#include "CardHandler.h"

#include "Arduino.h"
#include "SD.h"

#include "PlayObject.h"
#include "PlayObjectFolder.h"
#include "PlayObjectSingle.h"
#include "PlayObjectList.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "CardHandler";
#endif



 
//---------------------------------------------------------------------------------------
CardHandler::CardHandler(/* args */)
{
}


//---------------------------------------------------------------------------------------
std::vector<std::shared_ptr<CardObject>> CardHandler::getCardObject( RFCard *pCard )
{
    std::vector<std::shared_ptr<CardObject>> result;
    
    String filename = "/.cards/" + pCard->createFileName();

    log_v("Try to open TAG file \"%s\"", filename.c_str());

    StaticJsonDocument<512> doc;
                
    // try to load the file, deserialize it and create a mattching object
    if (loadJsonFile(filename, &doc))
    {
        JsonObject obj = doc.as<JsonObject>();
        JsonArray arr = obj["Files"];
        
        if (arr.size())
        {
            log_d("Array Size %d", arr.size());

            for (uint32_t counter = 0; counter < arr.size(); counter++)
            {
                String objectPath = arr.getElement(counter) | "";

                if (objectPath.length())
                {
                    log_d("File(%lu): \"%s\"", counter, objectPath.c_str());

                    result.push_back(CardHandler::getCardObject(objectPath));
                }
                else
                {
                    log_d("Entry %lu contained no data", counter);
                }
            }
        }
        else
        {
            log_w("JSON Array is empty (no files added)");
        }
    }
    else 
    {
        log_i("No Card definition file available");
    }

    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<CardObject> CardHandler::getCardObject( String filename ) 
{
    std::shared_ptr<CardObject> result = nullptr;

    // start checking the parameters 
    if ((filename) && (filename.length()))
    {
        if (filename.startsWith("/"))
        {            
            // determine if we've got the link to a json file (this could be a single file, a playlist or a folder)
            if (filename.substring(filename.length()-5).equalsIgnoreCase(".json"))
            {
                StaticJsonDocument<512> doc;
                
                // try to load the file, deserialize it and create a mattching object
                if (loadJsonFile(filename, &doc))
                {
                    JsonObject obj = doc.as<JsonObject>();

                    if (obj["Card"] == "Play")
                    {
                        JsonObject cardObj = obj["PlayObject"];

                        // create a single file object
                        if (obj["Type"] == "Single File")
                        {                                
                            result = initializePlaySingleObject(filename, &cardObj);
                        }
                        else if (obj["Type"] == "Folder")
                        {                                
                            result = initializePlayFolderObject(filename, &cardObj);
                        }
                        else if (obj["Type"] == "List")
                        {                                
                            result = initializePlayListObject(filename, &cardObj);
                        }
                        else
                        {
                            log_w("Card Type \"%s\" unknown", obj["Type"]);
                        }
                    }
                    else
                    {
                        ESP_LOGW(TAG, "Unknown card function");
                    }
                }
                else
                {
                    ESP_LOGW(TAG, "Loading json file failed!");
                }
            }
            // a playlist
            else if (filename.substring(filename.length()-4).equalsIgnoreCase(".m3u"))
            {
                //make sure the file exists
                if (SD.exists(filename))
                {
                    // check if a corresponding .json exists
                    if (SD.exists(filename + ".json"))
                    {
                        StaticJsonDocument<512> doc;

                        if (loadJsonFile(filename + ".json", &doc))
                        {
                            JsonObject obj = doc.as<JsonObject>();

                            if ((obj["Card"] == "Play")  && (obj["Type"] == "List"))
                            {
                                JsonObject cardObj = obj["PlayObject"];

                                result = initializePlayListObject(filename + ".json", &cardObj);
                            }
                            else
                            {
                                ESP_LOGW(TAG, "JSON did not match a m3u file");
                            }
                        }
                        else
                        {
                            ESP_LOGW(TAG, "could not open \"%s\"", filename + ".json");
                        }                        
                    }
                    // create play list play object
                    else
                    {
                        result = std::make_shared<PlayObjectList>();

                        if (result)
                        {
                            // try to initialize the Object
                            if (!std::static_pointer_cast<PlayObjectList>(result)->create(filename))
                            {
                                ESP_LOGW(TAG, "PlayObjectList create failed");

                                result = nullptr;
                            }
                            else
                            {
                                if (!updateCardObject(result))
                                {
                                    ESP_LOGW(TAG, "Failed to create new JSON file \"%s\"", filename);
                                }
                            }
                        }
                        else
                        {
                            ESP_LOGW(TAG, "could not create PlayObjectList");
                        }
                    }
                }
            }
            // a single mp3 file
            else if (filename.substring(filename.length()-4).equalsIgnoreCase(".mp3"))
            {
                //make sure the file exists
                if (SD.exists(filename))
                {
                    // check if a corresponding .json exists
                    if (SD.exists(filename + ".json"))
                    {
                        StaticJsonDocument<512> doc;

                        if (loadJsonFile(filename + ".json", &doc))
                        {
                            JsonObject obj = doc.as<JsonObject>();

                            if ((obj["Card"] == "Play")  && (obj["Type"] == "Single File"))
                            {
                                JsonObject cardObj = obj["PlayObject"];

                                result = initializePlaySingleObject(filename + ".json", &cardObj);
                            }
                            else
                            {
                                ESP_LOGW(TAG, "JSON did not match a single mp3 file");
                            }
                        }
                        else
                        {
                            ESP_LOGW(TAG, "could not open \"%s\"", filename + ".json");
                        }                        
                    }
                    // create single file play object
                    else
                    {
                        result = std::make_shared<PlayObjectSingle>();

                        if (result)
                        {
                            // try to initialize the Object
                            if (!std::static_pointer_cast<PlayObjectSingle>(result)->create(filename))
                            {
                                ESP_LOGW(TAG, "PlayObjectSingle create failed");

                                result = nullptr;
                            }
                            else
                            {
                                if (!updateCardObject(result))
                                {
                                    ESP_LOGW(TAG, "Failed to create new JSON file \"%s\"", filename);
                                }
                            }
                        }
                        else
                        {
                            ESP_LOGW(TAG, "could not create PlayObjectSingle");
                        }
                    }
                }
                else
                {
                    ESP_LOGW(TAG, "file \"%s\" does not exist", filename);
                }
            }
            
            // something else
            else 
            {
                // a folder name
                // else if (filename.substring(filename.length()-4).equalsIgnoreCase(".m3u"))
                // {

                // }

                // check if a corresponding .jsn exists
                
                // if (SD.exists(filename))
                // {

                // }
            //     // unkown
            //     else
            // {
            //     ESP_LOGW(TAG, "unknown file type");
            // }

            }
        }
        else
        {
            ESP_LOGW(TAG, "filename doesn't start with \"/\"");
        }
    }
//     if (jsonDoc && jsonFile)
//     {        
//         if (jsonFile.startsWith("/") && jsonFile.endsWith(".json"))
//         {
//             if (SD.exists(jsonFile))
//             {
//                 File json = SD.open(jsonFile);

//                 if (json)
//                 {
//                     DeserializationError error = deserializeJson(*jsonDoc, json);

//                     if (error) 
//                     {
//                         ESP_LOGE(TAG, "deserializeJson() failed: %s", error.c_str());
//                     }
//                     else
//                     {
//                         // remember the path to allow saving the object
//                         m_jsonFile = jsonFile;

//                         result = true;
//                     }

//                     json.close();
//                 }   
//                 else
//                 {
//                     ESP_LOGE(TAG, "cold not open the json file \"%s\"", jsonFile.c_str());
//                 }
//             }
//             else
//             {
//                 ESP_LOGW(TAG, "json file does not exist");
//             }
//         }
//         else
//         {
//             ESP_LOGW(TAG, "json file path invalid");
//         }

//     }
//     else
//     {
//         ESP_LOGW(TAG, "parameter missing");
//     }
//     // DynamicJsonDocument doc(200);

//     // // JSON input string.
//     // char json[] = "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";

//     // // Deserialize the JSON document
//     // DeserializationError error = deserializeJson(doc, json);

//     // // Test if parsing succeeds.
//     // if (error) {
//     //     Serial.print(F("deserializeJson() failed: "));
//     //     Serial.println(error.c_str());
//     //     return result;
//     // }
    
//     // if (doc["Card"].as<String>())
//     // {
//     //     // now we know the type
//     // }
//     //

//     return result;

    return result;
}


//---------------------------------------------------------------------------------------
bool CardHandler::updateCardObject( const std::shared_ptr<CardObject>& CardObject )
{
    bool result = false;

    StaticJsonDocument<512> doc;

    // add the card type to the json
    doc["Card"] = CardObject->GetCardTypeString();

    //
    if (CardObject->GetCardType() == CardObject::CardType::PLAY)
    {
        // create add the play card type to the json file
        doc["Type"] = std::static_pointer_cast<PlayObject>(CardObject)->getPlayTypeString();

        // add the data for the play card object
        JsonObject cardObj = doc.createNestedObject("PlayObject");

        if (CardObject->save(cardObj))
        {            
            result = true;
        }
        else
        {
            ESP_LOGW(TAG, "Failed to serialize play card");            
        }
    }
    else if (CardObject->GetCardType() == CardObject::CardType::MODIFICATOR)
    {

    }
    else
    {
        ESP_LOGW(TAG, "Unkown Card Type");
    }

    // if the conversion to json was successfull, write the data to the SD card
    if (result)
    {
        File jsonFile = SD.open(CardObject->getJsonFileName(), FILE_WRITE);

        if (jsonFile)
        {
            if (serializeJson(doc, jsonFile) > 0)
            {
                ESP_LOGD(TAG, "JSON file updated");
            }
            else
            {
                ESP_LOGW(TAG, "Failed to write JSON file");
            }
        }
        else 
        {
            ESP_LOGE(TAG, "Error opening JSON file");
        }
    }
    
    return result;
}


//---------------------------------------------------------------------------------------
bool CardHandler::linkRfCardToCardObject(RFCard& card, CardObject& cardObject)
{
    // check if the configuration directory exists (create it if not)
    if (!SD.exists("/.cards"))
    {                                
        if (!SD.mkdir("/.cards"))
        {
            ESP_LOGE(TAG, "Could not create card database directory");
        }
    }

    //

    return true;
}


//---------------------------------------------------------------------------------------
bool CardHandler::addCardObjectToRfCard(RFCard& card, CardObject& cardObject)
{
    return false;
}


//---------------------------------------------------------------------------------------
bool CardHandler::deleteCardObjectFromRfCard(RFCard& card, CardObject& cardObject)
{
    return false;
}


//---------------------------------------------------------------------------------------
bool CardHandler::loadJsonFile(const String& jsonFile, JsonDocument *jsonDoc)
{
    bool result = false;

    // start checking the parameters 
    if (jsonDoc && jsonFile)
    {
        // check if the path is valid
        if (jsonFile.startsWith("/") && jsonFile.endsWith(".json"))
        {
            // check if the files exists
            if (SD.exists(jsonFile))
            {                                
                // open the json file
                File json = SD.open(jsonFile);

                // if the file was successfully opened
                if (json)
                {

                    // load and parse the document
                    DeserializationError error = deserializeJson(*jsonDoc, json);

                    if (error) 
                    {                               
                        ESP_LOGE(TAG, "deserializeJson() failed: %s", error.c_str());
                    }
                    else 
                    {
                        result = true;
                    }

                    json.close();
                }
            }
        }
    }

    return result;
}



// #include "PlayObjectHandler.h"

//     #include "SD.h"
//     #include "FS.h"

// #ifdef ARDUINO_ARCH_ESP32
//     #include "esp32-hal-log.h"    
// #else
//     static const char *TAG = "CardHandler";
// #endif

// //---------------------------------------------------------------------------------------
// PlayObjectHandler::PlayObjectHandler(/* args */)
// {
// }


// //---------------------------------------------------------------------------------------
// PlayObjectHandler::~PlayObjectHandler()
// {
// }


// //---------------------------------------------------------------------------------------
// std::shared_ptr<PlayObject> PlayObjectHandler::getPlayObject( String filename )
// {
//     std::shared_ptr<PlayObject> result ;//= std::make_shared<PlayObjectSingle>();


//     //if the filename is not already a .json file call the create
//     if (!filename.endsWith(".json"))
//     {
//         //if the filename is valid, check if a json exists for this object
//         if (PlayObjectHandler::isFilenameValidObject(filename))
//         {
            
//         }


//         result = createPlayObject(filename + ".json", filename );
//         goto Finish;
//     }

//     //make sure the file exists (if not abort the process)
//     if (SD.exists(filename))
//     {
//         File file = SD.open(filename);

//         if (!file)
//         {
//             ESP_LOGE(TAG, "Error opening \"%s\"", filename.c_str());
//             goto Finish;
//         }
//         // load the json and deserialize it
//         else
//         {
//             // Allocate a temporary JsonDocument
//             StaticJsonDocument<512> doc;

//             // Deserialize the JSON document
//             DeserializationError error = deserializeJson(doc, file);
            
//             if (error)
//             {
//                 ESP_LOGE(TAG, "Failed to read file");
//                 file.close();
//                 goto Finish;
//             }

//             // decode the json file



//             file.close();
//         }
//     }
//     else
//     {
//         ESP_LOGE(TAG, "\"%s\" does not exists", filename.c_str());
//     }


// Finish:
//     return result;
// }


// //---------------------------------------------------------------------------------------
// std::shared_ptr<PlayObject> PlayObjectHandler::createPlayObject( String containerFileName, String objectFileName )
// {
//     std::shared_ptr<PlayObject> result ;//= std::make_shared<PlayObjectSingle>();

//     ESP_LOGD(TAG, "Create Container %s for file %s", containerFileName.c_str(), objectFileName.c_str());

//     return result; 
// }


// //---------------------------------------------------------------------------------------
// bool PlayObjectHandler::savePlayObject( PlayObject *pPlayObject )
// {
//     bool result = false;

    


//     return result;
// }


// //---------------------------------------------------------------------------------------
// bool PlayObjectHandler::isFilenameValidObject( String filename )
// {
//     bool result = false;


//     return result;
// }

// //---------------------------------------------------------------------------------------
// // std::shared_ptr<CardObject> CardHandler::GetCardObject( RFCard& card )
// // {

// //     // find the correct file and read the json string for the card id


// //     // // Allocate the JSON document
// //     // DynamicJsonDocument doc(200);

// //     // // JSON input string.
// //     // char json[] = "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";

// //     // // Deserialize the JSON document
// //     // DeserializationError error = deserializeJson(doc, json);

// //     // // Test if parsing succeeds.
// //     // if (error) {
// //     //     Serial.print(F("deserializeJson() failed: "));
// //     //     Serial.println(error.c_str());
// //     //     return result;
// //     // }
    
// //     // if (doc["Card"].as<String>())
// //     // {
// //     //     // now we know the type
// //     // }

// //     return result;
// // }


// //---------------------------------------------------------------------------------------
// // void        GetModificatorObject( void );

// 
// // bool UpdateCard(RFCard& card, CardObject& cardObject)
// // {
// //     bool result = false;

// //     String seriallized;

// //     // get the string represantation of the object
// //     seriallized = cardObject.Serialize();

// //     //TODO save the serialized object to the SD Card



// //     return result;
// // }

//---------------------------------------------------------------------------------------
std::shared_ptr<CardObject> CardHandler::initializePlaySingleObject(const String& filename, JsonObject *obj )
{
    // create a specific result
    std::shared_ptr<PlayObjectSingle> result = std::make_shared<PlayObjectSingle>();

    // and try to fill it with the data from the json
    if (!result->initialize(filename, *obj))
    {
        result = nullptr;
        ESP_LOGE(TAG, "Could not initialize SingleFile from JSON");                                    
    }
    else 
    {
        ESP_LOGD(TAG, "Load Single File from JSON");
    }

    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<CardObject> CardHandler::initializePlayFolderObject(const String& fileName, JsonObject *obj )
{
    std::shared_ptr<PlayObjectFolder> result = nullptr;//std::make_shared<PlayObjectSingle>();                                


    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<CardObject> CardHandler::initializePlayListObject(const String& filename, JsonObject *obj )
{
    // create a specific result
    std::shared_ptr<PlayObjectList> result = std::make_shared<PlayObjectList>();

    // and try to fill it with the data from the json
    if (!result->initialize(filename, *obj))
    {
        result = nullptr;
        ESP_LOGE(TAG, "Could not initialize ListFile from JSON");                                    
    }
    else 
    {
        ESP_LOGD(TAG, "Load List from JSON");
    }

    return result;
}