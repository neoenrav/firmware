/** @file CommandLineInterface.h
 * 
 * @brief Thread to monitor the serial interface and handle several commands.
 *
 * 
 * Licensed under GNU GPLv3 <http://gplv3.fsf.org/>
 * Copyright © 2017
 *
 * @authors enrav, asealion,
 *
 * Development log:
 *  - 2019: first version 
 *          by Mike Reinecke (github: @asealion )
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License or later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 


#ifndef LIB_COMMANDLINEINTERFACE_SRC_COMMANDLINEINTERFACE_H_  
    #define LIB_COMMANDLINEINTERFACE_SRC_COMMANDLINEINTERFACE_H_  

    #include "thread.hpp"
    #include "SimpleCLI.h"
    #include "Stream.h"    

    #include "Player.h"
    #include "Network.h"
    

   /*! \brief The CommandLineInterface (CLI) could be useded to 
    *           control, monitor and debug the different EnRav functions
    *
    *  
    */
    class CommandLineInterface: public cpp_freertos::Thread
    {
        public:
            CommandLineInterface(/* args */);
            ~CommandLineInterface();

            // sets pins correctly and prepares SPI bus.
            void        connect( Stream *pStream ) ; 

            //
            bool        attach( Player *pPlayer);
            bool        attach( Network *pNetwork);

            // try to start the thread ( only possible if it is configured/connected )
            bool        Start( void );       

        protected:
        
            void        Run( void );                                // the "real" thread function

        private:
            SimpleCLI   cli;  

            Stream      *m_Stream;

            Player      *m_Player;
            Network     *m_Network;

            //Player related commands
            Command     m_cmdPlayer;                                // Player configurations

            Command     m_cmdPlay;
            Command     m_qryVolume;
            Command     m_cmdVolume;

            Command     m_cmdChangeDirectory;
            Command     m_cmdCatFile;

            Command     m_cmdNetworkEnable;
            Command     m_cmdNetworkFTP;
            Command     m_cmdNetworkWebServer;
            Command     m_cmdNetworkMQTT;

            String      m_File;

        private:

            void        Command_Help(Command cmd);

            void        Command_List(Command cmd);
            void        Command_ChangeDirectory(Command cmd);
            void        Command_CatFile(Command cmd);

            void        Query_Player(Command cmd);
            void        Command_Player(Command cmd);
            void        Command_Play(Command cmd);
            void        Command_Pause(Command cmd);
            void        Command_Reset(Command cmd);

            void        Command_VolumeQry(Command cmd);
            void        Command_VolumeCmd(Command cmd);

            void        Query_WiFi(Command cmd);
            void        Command_WiFi(Command cmd);
    }; 

#endif // LIB_COMMANDLINEINTERFACE_SRC_COMMANDLINEINTERFACE_H_  