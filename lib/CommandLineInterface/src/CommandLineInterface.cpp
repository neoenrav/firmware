#include "CommandLineInterface.h"

#include "SD.h"
#include "FS.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "CLI";
#endif


//---------------------------------------------------------------------------------------
CommandLineInterface::CommandLineInterface(/* args */):
    Thread("CLI", 8192, tskIDLE_PRIORITY),
    m_Stream(NULL),
    m_Player(NULL),
    m_Network(NULL),    
    m_File("/")

{
}


//---------------------------------------------------------------------------------------
CommandLineInterface::~CommandLineInterface()
{
}


//---------------------------------------------------------------------------------------
void CommandLineInterface::connect( Stream *pStream )
{
    m_Stream = pStream;
}

//---------------------------------------------------------------------------------------
bool CommandLineInterface::attach( Player *pPlayer)
{
    bool result = false;

    // TODO(me) only possible if the thread isn't already started
    if (pPlayer)
    {
        this->m_Player = pPlayer;

        result = true;
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool CommandLineInterface::attach( Network *pNetwork)
{
    bool result = false;

    // TODO(me) only possible if the thread isn't already started
    if (pNetwork)
    {
        this->m_Network = pNetwork;

        result = true;
    }

    return result;
}

//---------------------------------------------------------------------------------------
bool CommandLineInterface::Start( void )
{
    bool result = true;

    //check the different modules
    if (m_Stream == NULL)
    {
        ESP_LOGW(TAG, "No stream connected");
        result = false;
    }

    if (result)
    {
        result = Thread::Start();
    }

    return result;
}


//---------------------------------------------------------------------------------------
void CommandLineInterface::Run( void )
{
    String input;

    // setup the CLI <basic commands>
    cli.addCommand("help");    
    cli.addCommand("version");

    // command to move around the file system
    cli.addCommand("ls");
    m_cmdChangeDirectory = cli.addCommand("cd");
    m_cmdChangeDirectory.addPositionalArgument("directory");
    m_cmdCatFile = cli.addCommand("cat");
    m_cmdCatFile.addPositionalArgument("file");

    // query runtime parameter
    cli.addCommand("heap?");

    //if a player is attached, we could execute the following commands
    if (m_Player)
    {
        cli.addCommand("player?");
        m_cmdPlayer = cli.addCommand("player");
        m_cmdPlayer.addArgument("stop", "").isOptional();

        m_cmdPlay = cli.addCommand("play");
        m_cmdPlay.addPositionalArgument("file");
        m_cmdPlay.addFlagArgument("r/epeat");

        cli.addCommand("pause");
        cli.addCommand("reset");

        // Volume
        m_qryVolume = cli.addCommand("volume?");
        m_cmdVolume = cli.addCommand("volume");
        m_cmdVolume.addPositionalArgument("value", "keep");
        m_cmdVolume.addArgument("max", "keep").isOptional();
        m_cmdVolume.addArgument("default", "keep").isOptional();

    }

    if (m_Network)
    {
        cli.addCommand("WIFI?");
        m_cmdNetworkEnable = cli.addCommand("WIFI");
        m_cmdNetworkEnable.addPositionalArgument("enable", "trigger").isOptional();
        m_cmdNetworkEnable.addArgument("ssid", "ssid").isOptional();
        m_cmdNetworkEnable.addArgument("pwd,password", "pwd").isOptional();
        m_cmdNetworkEnable.addArgument("time/out", "").isOptional();

        cli.addCommand("FTP?");
        m_cmdNetworkFTP = cli.addCommand("FTP");
        m_cmdNetworkFTP.addPositionalArgument("enable", "true");
        m_cmdNetworkFTP.addArgument("u/ser", "").isOptional();
        m_cmdNetworkFTP.addArgument("pwd,password", "").isOptional();
        m_cmdNetworkFTP.addArgument("port", "21").isOptional();
    }

    while (1)
    {
        // give the user a command prompt with the actual path
        m_Stream->printf("\u001b[0;34m%s\u001b[0m$ ", m_File.c_str());

        input.clear();

        // collect charsters from the stream until we receive \n
        while (1) 
        {
            if (m_Stream->available())
            {
                char c = m_Stream->read();

                // this is our command termination character
                if (c == '\n')
                {
                    break;
                }                
                else if (c == '\b')
                {
                    if(input.length())
                    {
                        input = input.substring(0, input.length()-1);
                    }

                    m_Stream->write('\b');
                    m_Stream->write(' ');
                    m_Stream->write('\b');
                }
                else                
                {
                    input += c;

                    m_Stream->write(c);
                }                
            } 
            else 
            {
                DelayMS(10);
            }
        } 

        input.trim();

        cli.parse(input);

        m_Stream->println();

        // Check for newly parsed commands
        if (cli.available()) 
        {
            // Get command out of queue
            Command cmd = cli.getCmd();

            // React on our commands
            if (cmd.equals("help")) 
            {
                Command_Help(cmd);
            }
            else if (cmd.equals("version"))
            {                
                m_Stream->print(VERSION_MAJOR);
                m_Stream->print(".");
                m_Stream->print(VERSION_MINOR);
                m_Stream->print(".");
                m_Stream->println(VERSION_PATCH);
            }
            else if (cmd.equals("ls"))
            {
                Command_List(cmd);
            }
            else if (cmd.equals("cd"))
            {
                Command_ChangeDirectory(cmd);
            }
            else if (cmd.equals("cat"))
            {
                Command_CatFile(cmd);
            }
            else if (cmd.equals("heap?"))
            {
                m_Stream->print("Heap: ");
                m_Stream->print(ESP.getFreeHeap()/1024);
                m_Stream->print("kB of ");
                m_Stream->print(ESP.getHeapSize()/1024);
                m_Stream->println("kB free");
            }
//NETWORK
            else if (cmd.equals("WIFI?"))
            {
                Query_WiFi(cmd);
            }
            else if (cmd.equals("WIFI"))
            {
                Command_WiFi(cmd);
            }
            else if (cmd.equals("FTP"))
            {
                String enable = cmd.getArgument("enable").getValue();
                String user = cmd.getArgument("user").getValue();
                String password = cmd.getArgument("pwd").getValue();
                String port = cmd.getArgument("port").getValue();

                // check if the ftp service should be enabled
                if (enable.equalsIgnoreCase("true"))
                {
                    bool trigger = false;

                    // check if a "new" user is set
                    if (!user.isEmpty())
                    {
                        // set user
                    }

                    if (!password.isEmpty())
                    {
                        // set password
                    }

                    if (!port.equalsIgnoreCase("21"))                    
                    {
                        int32_t newPort = port.toInt();

                        if (newPort == 0)
                        {
                            m_Stream->printf("Invalid port \"%s\"", port.c_str());
                        }
                        else
                        {
                            //set new port
                        }
                    }

                    // if ftp is off, enable it
                    if (0)
                    {
                        //

                        // trigger an update of the network service
                        m_Network->sendTrigger();
                    }

                }

                ESP_LOGD(TAG, "FTP Command : enable %s, user %s, password %s, port %s ", enable, user, password, port);
            }

//PLAYER
            else if (cmd.equals("player?"))
            {
                Query_Player(cmd);
            }
            else if (cmd == m_cmdPlayer)
            {
                Command_Player(cmd);
            }
            else if (cmd == m_cmdPlay)
            {           
                Command_Play(cmd);
            }
            else if (cmd.equals("pause"))
            {
                Command_Pause(cmd);
            }
            else if (cmd.equals("reset"))
            {
                Command_Reset(cmd);
            }
            else if (cmd == m_qryVolume)
            {
                Command_VolumeQry(cmd);
            }
            else if (cmd == m_cmdVolume)
            {
                Command_VolumeCmd(cmd);
            }

        }

        // Check for parsing errors
        if (cli.errored()) {
            // Get error out of queue
            CommandError cmdError = cli.getError();

            // Print the error
            Serial.print("ERROR: ");
            Serial.println(cmdError.toString());

            // Print correct command structure
            if (cmdError.hasCommand()) {
                Serial.print("Did you mean \"");
                Serial.print(cmdError.getCommand().toString());
                Serial.println("\"?");
            }
        }
    }
}


//---------------------------------------------------------------------------------------
void CommandLineInterface::Command_Help(Command cmd)
{
    m_Stream->println("EnRav Audio player (by M. Reinecke)");
    m_Stream->println("-----------------------------------");
    m_Stream->println("available commands:");
    m_Stream->println("- version                        : show firmware version");
    m_Stream->println("- heap?                          : show available and total RAM");
    m_Stream->println("");

    if (m_Player)
    {
        m_Stream->println("- play <filename>  [-repeat]     : start playing the given file name");
        m_Stream->println("                                     from the beginning (must be a mp3 or m3u file)");
        m_Stream->println("- pause                          : ");
        m_Stream->println("- resume <filename>              : start playing the given file name");
        m_Stream->println("                                     from previous position (must be a mp3 or m3u file)");
        m_Stream->println("- stop                           : stops the actual playback");
        m_Stream->println("");
        m_Stream->println("- player?                        : query the settings for the player");
        m_Stream->println("- player [-stop <true|false>]    : change player settings");
        m_Stream->println("                                     stop -> stop playback when teh card is removed");
        m_Stream->println("");
        m_Stream->println("- volume?                        : query actual volume settings");
        m_Stream->println("- volume [<1..10>|<UP>|<DOWN>] ");
        m_Stream->println("                                 : set volume to level");
        m_Stream->println("                                     <UP> increment volume one steps");
        m_Stream->println("                                     <DOWN> decrement volume one steps");


    }
    //checked
    if (m_Network)
    {
        m_Stream->println("- WIFI?                          : query all WiFi parameters");
        m_Stream->println("- WIFI <ON/OFF/AUTO/TIGGER> [-ssid <clear>] [-pwd <clear>] [-timeout <min>]");
        m_Stream->println("                                 : use \"clear\" to delete saved parameter");
    }


    m_Stream->println(" - write <filename>              : setup RFID card with the given parameters");
}


//---------------------------------------------------------------------------------------
void CommandLineInterface::Command_List(Command cmd)
{
    File folder;

    folder = SD.open(m_File);

    if (folder)
    {
        
        while (true) 
        {
            File entry =  folder.openNextFile();

            if (! entry) 
            {
                // no more files
                break;
            }

            if (entry.isDirectory()) 
            {
                m_Stream->print(entry.name());
                m_Stream->println("/");
            }
            entry.close();  
        }

        folder.rewindDirectory();

        while (true) 
        {
            File entry =  folder.openNextFile();

            if (! entry) 
            {
                // no more files
                break;
            }

            if (!entry.isDirectory()) 
            {
                m_Stream->print(entry.name());

                // files have sizes, directories do not
                m_Stream->print("\t\t");
                m_Stream->println(entry.size(), DEC);
            }
            entry.close();  
        }

        folder.close();
    }
}


void CommandLineInterface::Command_ChangeDirectory(Command cmd)
{
    String directory = cmd.getArg("directory").getValue();

    // change one level up
    if (directory.equals(".."))
    {
        if (!m_File.equals("/"))
        {
            // "remove" the deepest directory
            String newFile = m_File.substring(0,m_File.lastIndexOf('/'));

            m_File = newFile;
        }
    }
    // change into root directory
    else if (directory.equals("/"))
    {
        m_File = "/";
    }
    // try to change the directory
    else
    {
        String newPath = m_File + "/" + directory;

        if (SD.exists(newPath))
        {
            File newFile = SD.open(newPath);

            if (newFile)
            {
                if (newFile.isDirectory())
                {
                    m_File = newPath;
                }
                else
                {
                    m_Stream->print("cd: ");
                    m_Stream->print(directory);
                    m_Stream->println(": Not a directory");
                }

                newFile.close();
            }
        }
        else
        {
            m_Stream->print("cd: ");
            m_Stream->print(directory);
            m_Stream->println(": No such file or directory");
        }
    }
}


void CommandLineInterface::Command_CatFile(Command cmd)
{

    String path = cmd.getArg("file").getValue();
    File   myFile;

    path.trim();

    if (path.charAt(0) != '/')
    {
        path = m_File + "/" + path;
    }

    if (SD.exists(path))
    {
        myFile = SD.open(path);

        if (myFile)
        {        
            while (myFile.available()) 
            {
                m_Stream->printf("%c", myFile.read());
            }
            myFile.close();

            m_Stream->println();
        }
        else
        {
            m_Stream->printf("could not open file \"%s\"", path.c_str());
        }
    }
    else
    {
        m_Stream->println("file not found");
    }
}

//---------------------------------------------------------------------------------------
void CommandLineInterface::Query_Player(Command cmd)
{
    m_Stream->print("-stop <");
    if (m_Player->getStopOnCardRemove())
    {
        m_Stream->print("true");
    }
    else
    {
        m_Stream->print("false");
    }
    m_Stream->print(">");

    m_Stream->println();
}

void CommandLineInterface::Command_Player(Command cmd)
{
    bool result = false;
    String stop = cmd.getArgument("stop").getValue();

    // check the given parameters

    // "stop" on card remove
    if (stop.equalsIgnoreCase("true"))
    {
        m_Player->setStopOnCardRemove(true);
    }
    else if (stop.equalsIgnoreCase("false"))
    {
        m_Player->setStopOnCardRemove(false);
    }
    else
    {
        m_Stream->printf("invalid parameter \"%s\" for \"stop\" ", stop.c_str());
    }

    if (result)
    {
        m_Stream->println();
    }
    
}

void CommandLineInterface::Command_Play(Command cmd)
{
    Argument file = cmd.getArg("file");

    String filename = file.getValue();

    if (SD.exists(filename))
    {
        m_Player->PlayFile(filename);        

        // m_Stream->print("Play Song ");
        // if(cmd.getArgument("repeat").isSet()) m_Stream->print("neverending ");
        // m_Stream->println(file.getValue());
    }
    else
    {
        m_Stream->print("Invalid path: ");
        m_Stream->println(filename);
    }
}

void CommandLineInterface::Command_Pause(Command cmd)
{
    if (m_Player->PlayerPauseResume())
    {
        m_Stream->println("Ok");
    }
    else
    {
        m_Stream->println("Error");
    }
}

void CommandLineInterface::Command_Reset(Command cmd)
{
    if (m_Player->PlayerReset())
    {
        m_Stream->println("Ok");
    }
    else
    {
        m_Stream->println("Error");
    }
}

void CommandLineInterface::Command_VolumeQry(Command cmd)
{
    m_Stream->printf("actual: %u (default %u, maximum %u)", m_Player->getVolume(), m_Player->getDefaultVolume(), m_Player->getMaximumVolume());
    m_Stream->println("");
}

void CommandLineInterface::Command_VolumeCmd(Command cmd)
{
    String value  = cmd.getArg("value").getValue();
    if (!value.equalsIgnoreCase("keep"))
    {
        if (value.equalsIgnoreCase("UP") || value.equals("++"))
        {
            m_Player->setVolume(m_Player->getVolume() + 1);
        }
        else if (value.equalsIgnoreCase("DOWN") || value.equals("--"))
        {
            m_Player->setVolume(m_Player->getVolume() - 1);
        }
        else
        {
            int32_t num = value.toInt();

            if (!m_Player->setVolume(num))
            {
                m_Stream->println("illegal value");
            }        
        }
    }

    String max = cmd.getArg("max").getValue();
    if (!max.equals("keep"))
    {
        int32_t num = max.toInt();

        if (!m_Player->setMaximumVolume(num))
        {
            m_Stream->println("illegal max value");
        }
    }

    String def = cmd.getArg("default").getValue();    
    if (!def.equals("keep"))
    {
        int32_t num = def.toInt();

        if (!m_Player->setDefaultVolume(num))
        {
            m_Stream->println("illegal default value");
        }
    }
}

//---------------------------------------------------------------------------------------
void CommandLineInterface::Query_WiFi(Command cmd)
{
    if (m_Network->getActive())
    {
        if (m_Network->getMode() == Network::NetworkMode::AUTO)
        {
            m_Stream->print("<AUTO>");
        }
        else
        {
            m_Stream->print("<ON>");
        }
    }
    else
    {
        m_Stream->print("<OFF>");
    }

    m_Stream->printf(" ssid: \"%s\" pwd: \"%s\" timeout: %umin" , m_Network->getSSID().c_str(), m_Network->getWifiPassword().c_str(), m_Network->getTimeout());

    m_Stream->println();
}

void CommandLineInterface::Command_WiFi(Command cmd)
{

    String enable   = cmd.getArgument("enable").getValue();

    if (enable.equalsIgnoreCase("ON"))
    {
        m_Network->setMode(Network::NetworkMode::ENABLED);
    }
    else if (enable.equalsIgnoreCase("OFF"))
    {
        m_Network->setMode(Network::NetworkMode::OFF);
    }
    else if (enable.equalsIgnoreCase("AUTO"))
    {
        m_Network->setMode(Network::NetworkMode::AUTO);
    }
    else if (enable.equalsIgnoreCase("trigger"))
    {
        m_Network->sendTrigger();
    }
    else
    {
        m_Stream->print("Bad <Status> Parameter");
    }

    Argument ssid = cmd.getArgument("ssid");

    if (ssid.isSet())
    {
        String ssid_data = ssid.getValue();

        if (ssid_data.equals("clear"))
        {
            ssid_data = "";
        }

        m_Network->setSSID(ssid_data);
    }

    Argument pwd = cmd.getArgument("pwd,password");

    if (pwd.isSet())
    {
        String pwd_data = pwd.getValue();

        if (pwd_data.equals("clear"))
        {
            pwd_data = "";
        }

        m_Network->setWifiPassword(pwd_data);
    }

    Argument timeout = cmd.getArgument("time/out");

    if (timeout.isSet())
    {
        int32_t timeout_data = timeout.getValue().toInt();

        if ((timeout_data >= 1) && (timeout_data <= 60))
        {
            m_Network->setTimeout(timeout_data);
        }
        else
        {
            m_Stream->print("valid timeout 1<=x<=60 ");
        }
    }
}