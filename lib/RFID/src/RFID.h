/** @file RFID.h
 * 
 * @brief Thread to monitor a RFID chip, sends an event to the Player
 * if a new token is detected or the actual token is removed
 *
 * 
 * Licensed under GNU GPLv3 <http://gplv3.fsf.org/>
 * Copyright © 2017
 *
 * @authors enrav, asealion,
 *
 * Development log:
 *  - 2019: first version with MRCF522 binding
 *          by Mike Reinecke (github: @asealion )
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License or later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef RFID_H
    #define RFID_H

    #include <memory>
    #include "thread.hpp"

    #include "SPI.h"
    #include "MFRC522.h"


    /*! \brief The software representation of a RFID (MIFARE) card
        *
        *  Detailed description starts here.
        */
    class RFCard
    {
        public:
            RFCard(MFRC522::Uid *pUid);
            ~RFCard();

            MFRC522::Uid* getUID( void ) { return &m_Uid; };

            String        toString();
            String        createFileName();

        private:
            MFRC522::Uid     m_Uid;

    };

    /*! \brief Handles the communication with the RFID Module
        *
        *  Detailed description starts here.
        */
    class RFID : public cpp_freertos::Thread
    {
        public:

            /** @brief Set this to true to get several status messages
             * 
             * */
            bool        m_ModuleVerbose;

            /** @brief Set this to true (in combination with verbose) to debug the library
             * 
             * */
            bool        m_ModuleDebug;


        public:
            RFID( SPIClass *pSPI = &SPI, uint8_t cs = NOT_A_PIN, uint8_t rst = NOT_A_PIN, uint8_t irq = NOT_A_PIN );
            ~RFID();

            // sets pins correctly and prepares SPI bus.
            void        Connect( SPIClass *pSPI, uint8_t cs, uint8_t rst, uint8_t irq = NOT_A_PIN );

            // try to start the thread ( only possible if it is configured/connected )
            bool        Start( void );   


            /** @brief
             * 
             * */
            bool        registerCardDetected(TaskHandle_t task, uint32_t mask);
            
            /** @brief
             * 
             * */
            bool        registerCardRemoved(TaskHandle_t task, uint32_t mask);


            /*! \brief 
                *
                *   This methon will only write the data if the given UID is equal 
                *   to the UID of the cord on the reader.
                */
            std::shared_ptr<RFCard> getCard( void );

        protected:
        
            void        Run( void );                                // the "real" thread function

        private:
            enum class RfidCardStatus { NoCard, ValidCard, UnknownCard };

            RfidCardStatus          m_CardStatus;                   /// our internal card status

            std::shared_ptr<RFCard> m_ActualCard;         

            struct TaskNotificationFlag {
                TaskHandle_t handle;
                uint32_t bitValue;
            };

            std::vector<TaskNotificationFlag>   m_NewCardVector;
            std::vector<TaskNotificationFlag>   m_CardRemovedVector;

            struct Configuration                                    /// configuration for the hardware connection
            {                
                SPIClass        *pSpi;

                struct Pins
                {
                    uint8_t         ChipSelect;                     // Pin where CS line is connected
                    uint8_t         Reset;                          // Pin where Reset is connected
                    uint8_t         Irq;                            // IRQ line
                }               Pins;

                SPISettings     Settings;                           // SPI settings for this slave

                bool            Configured;
            }               Configuration;


            MFRC522        *m_Reader;

        private:
            /* internal functions */
            bool isNewCardPresent( void );            
            bool isCardPresent( RFCard *pRCard );

            std::shared_ptr<RFCard> readCard( void );

            void stopCommunication( void );
    };


#endif