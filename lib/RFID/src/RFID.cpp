#include "Arduino.h"

#include "RFID.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "RFID";
#endif

//---------------------------------------------------------------------------------------
RFCard::RFCard(MFRC522::Uid *pUid) 
{     
    uint32_t uid_size = sizeof(MFRC522::Uid);

    memcpy(&m_Uid, pUid, uid_size);    
}


//---------------------------------------------------------------------------------------
RFCard::~RFCard() 
{
}


//---------------------------------------------------------------------------------------
String RFCard::toString(void) 
{
    String result = "[";

    result += "0x" + String(m_Uid.uidByte[0], 16);

    for (uint32_t counter = 1; counter < m_Uid.size; counter++)
    {
        result += " 0x" + String(m_Uid.uidByte[counter], 16);
    }

    result += "]";

    return result;
}


//---------------------------------------------------------------------------------------
String RFCard::createFileName(void) 
{
    String result = "t";
    

    for (uint32_t counter = 0; counter < m_Uid.size; counter++)
    {
        char buffer[3];
        snprintf(buffer, 3, "%02x", m_Uid.uidByte[counter]);

        result += String(buffer);
    }

    result += ".json";

    return result;
}


static xTaskHandle xRFIDTask;

//---------------------------------------------------------------------------------------
RFID::RFID(SPIClass *pSPI, uint8_t cs, uint8_t rst, uint8_t irq):
    Thread("RFID", 4096, tskIDLE_PRIORITY),
    m_ModuleVerbose(true),
    m_ModuleDebug(true),
    m_CardStatus(RfidCardStatus::NoCard),
    m_ActualCard(nullptr)
{

    // set the internal conficuration
    Connect(pSPI, cs, rst, irq);

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGD(TAG, "Constructor complete");
    }
}


//---------------------------------------------------------------------------------------
RFID::~RFID()
{
}


//---------------------------------------------------------------------------------------
void RFID::Connect( SPIClass *pSPI, uint8_t cs, uint8_t rst, uint8_t irq)
{
    Configuration.pSpi              = pSPI;

    Configuration.Pins.ChipSelect   = cs;
    Configuration.Pins.Reset        = rst;
    Configuration.Pins.Irq          = irq;

    // check if all necessary configurations are made
    if ((Configuration.Pins.ChipSelect == NOT_A_PIN) || (Configuration.Pins.Reset == NOT_A_PIN))
    {
        Configuration.Configured = false;
    }
    else
    {
        Configuration.Configured = true;
    }
}

//---------------------------------------------------------------------------------------
bool RFID::Start( void )
{
    bool result = false;
    
    if (Configuration.Configured)
    {
        pinMode(Configuration.Pins.ChipSelect, OUTPUT);      // 
        pinMode(Configuration.Pins.Reset, OUTPUT);

        digitalWrite(Configuration.Pins.ChipSelect, HIGH);
        digitalWrite(Configuration.Pins.Reset, HIGH);

        if (Configuration.Pins.Irq != NOT_A_PIN)
        {
            pinMode(Configuration.Pins.Irq, INPUT);      // 
            // attachInterrupt(Configuration.Pins.DataRequest, DreqIsr, RISING);
        }

        // start the thread, if all necessary configurations are set
        if (Configuration.pSpi->bus())
        {

            m_Reader = new MFRC522(Configuration.Pins.ChipSelect, Configuration.Pins.Reset);

            if (m_Reader)
            {

                result = Thread::Start();

                xRFIDTask = Thread::GetHandle();
            }
            else
            {
                ESP_LOGE(TAG, "Failed to create Reader");
            }
        }
        else 
        {
            ESP_LOGI(TAG, "SPI interface not ready");
        }
    }
    else
    {
        ESP_LOGI(TAG, "Connection not configured");
    }    

    return result;
}


//---------------------------------------------------------------------------------------
void RFID::Run( void )
{
    String myVersion;

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGD(TAG, "Connecting RFID reader...");
    }

    m_Reader->PCD_Init();                // Init MFRC522

	// Get the MFRC522 firmware version
	byte v = m_Reader->PCD_ReadRegister(MFRC522::VersionReg);

	// Lookup which version
	switch(v) {
		case 0x88: myVersion = String("(clone)");            break;
		case 0x90: myVersion = String("v0.0");               break;
		case 0x91: myVersion = String("v1.0");               break;
		case 0x92: myVersion = String("v2.0");               break;
		case 0x12: myVersion = String("counterfeit chip");   break;
		default:   myVersion = String("(unknown)");
	}

    if ((this->m_ModuleDebug) && (this->m_ModuleVerbose))
    {
        ESP_LOGI(TAG, "Firmware Version: 0x%02x = %s", v, myVersion.c_str());
    }
	

	// When 0x00 or 0xFF is returned, communication probably failed
	if ((v == 0x00) || (v == 0xFF))
    {
	    ESP_LOGW(TAG, "WARNING: Communication failure, is the MFRC522 properly connected?");
    }

    while (1)
    {

        if (m_CardStatus == RfidCardStatus::NoCard)
        {
            // wait for a card
            while ( isNewCardPresent() == false)
            {
                // wait some time before the check again for a card
                DelayMS(150);
            }            

            // read the card ad create m_ActualCard
            m_ActualCard = readCard();

            // set the correct status
            m_CardStatus = RfidCardStatus::ValidCard;

            if (m_ModuleVerbose)
            {
                ESP_LOGD(TAG, "card %s detected", m_ActualCard->toString().c_str());
            }

            // send events to listeners 
            if (m_NewCardVector.size())
            {
                for (std::vector<TaskNotificationFlag>::iterator tasksToNotify = m_NewCardVector.begin(); tasksToNotify != m_NewCardVector.end(); ++tasksToNotify)
                {
                    xTaskNotify(tasksToNotify->handle, tasksToNotify->bitValue, eSetBits);
                }

                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "send out new card notifications to other tasks");
                }
            }

            // send the card to sleep
            stopCommunication();

            // check if card is still there
            while (isCardPresent(m_ActualCard.get()) == true)
            {
                // send the card back to sleep
                stopCommunication();
               
                DelayMS(1000);
            }

            m_CardStatus = RfidCardStatus::NoCard;

            if (m_ModuleVerbose)
            {
                ESP_LOGD(TAG, "card %s removed", m_ActualCard->toString().c_str());
            }

            // send events to listeners 
            if (m_CardRemovedVector.size())
            {
                for (std::vector<TaskNotificationFlag>::iterator tasksToNotify = m_CardRemovedVector.begin(); tasksToNotify != m_CardRemovedVector.end(); ++tasksToNotify)
                {
                    xTaskNotify(tasksToNotify->handle, tasksToNotify->bitValue, eSetBits);
                }

                if ((m_ModuleVerbose) && (m_ModuleDebug))
                {
                    ESP_LOGD(TAG, "send out card removed notifications to other tasks");
                }
            }

            // forget about the card
            m_ActualCard = nullptr;
        }

        DelayMS(125);
    }
}


//---------------------------------------------------------------------------------------
bool RFID::registerCardDetected(TaskHandle_t task, uint32_t mask)
{
    bool result = false;

    if (task && mask)
    {
        m_NewCardVector.push_back({task, mask});

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
bool RFID::registerCardRemoved(TaskHandle_t task, uint32_t mask)
{
    bool result = false;

    if (task && mask)
    {
        m_CardRemovedVector.push_back({task, mask});

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
std::shared_ptr<RFCard> RFID::getCard( void )
{
    return m_ActualCard;
}


//---------------------------------------------------------------------------------------
bool RFID::isNewCardPresent( void )
{
    bool result = false;

    // Look for new card
    if ( m_Reader->PICC_IsNewCardPresent() == true) 
    {
        result = true;
    }

    return result;    
}


//---------------------------------------------------------------------------------------
bool RFID::isCardPresent( RFCard *pRfCard )
{
    bool result = false;

    // Since wireless communication is voodoo we'll give it a few retries before the card is gone
    for (uint32_t counter = 0; counter < 3; counter++) 
    {
        // Detect Tag without looking for collisions
        byte bufferATQA[2];
        byte bufferSize = sizeof(bufferATQA);

        MFRC522::StatusCode status = m_Reader->PICC_WakeupA(bufferATQA, &bufferSize);

        if (status == MFRC522::STATUS_OK)
        {
            if (m_Reader->PICC_ReadCardSerial()) 
            {
                bool uidEqual = true;

                //compare if the Uids have the same size
                if (pRfCard->getUID()->size == m_Reader->uid.size)
                {

                    //check the different bytes
                    for (uint32_t counter = 0; counter < pRfCard->getUID()->size; counter++)
                    {
                        if (m_Reader->uid.uidByte[counter] != pRfCard->getUID()->uidByte[counter])
                        {
                            uidEqual = false;
                            break;
                        }
                    }

                    if (uidEqual == true) 
                    {
                        result = true;
                        break;
                    }
                }
            }
        }
    } // "magic loop"

    return result;    
}


//---------------------------------------------------------------------------------------
std::shared_ptr<RFCard> RFID::readCard( void )
{
    std::shared_ptr<RFCard> result = nullptr;

    // Read the serial of one card
    if ( m_Reader->PICC_ReadCardSerial())
    {
        result = std::make_shared<RFCard>(&(m_Reader->uid));
    }

    return result;
}


//---------------------------------------------------------------------------------------
void RFID::stopCommunication(void)
{
    //end communication with the card
    m_Reader->PICC_HaltA();
    m_Reader->PCD_StopCrypto1();
}