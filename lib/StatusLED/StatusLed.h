#ifndef _STATUS_LED__H
    #define _STATUS_LED__H

    #include "Preferences.h"

    #include "thread.hpp"
    #include "queue.hpp"
    #include "mutex.hpp"

class StatusLed : public cpp_freertos::Thread
{
    public:
        StatusLed();
        virtual ~StatusLed();

        /** @brief Lists the different available patterns for the visualization
         * 
         * */
        enum class Pattern {
            EMPTY,                  // nothing (not setable)
            IDLE,                   // IDLE: four LEDs slow rotating
            ERROR,                  // ERROR: all LEDs flashing red (1x)
            OK,                     // OK: all LEDs flashing green (1x)
            BUSY,                   // BUSY: violet; four fast rotating LEDs
            TRACK_PROGRESS,         // track-progress: rainbow; number of LEDs relative to play-progress
            PLAYLIST_PROGRESS,      // playlist-progress: blue; appears only shortly in playlist-mode with every new track; number of LEDs relative to progress
            VOLUME,                 // volume: green => red-gradient; number of LEDs relative from actual to max volume
            // switching off: red; circle that grows until long-press-time is reached
            LOCKED,                 // buttons locked: track-progress-LEDs coloured red
            PAUSE,                  // paused: track-progress-LEDs coloured orange
            // rewind: if single-track-loop is activated a LED-rewind is performed when restarting the given track
            FAULT,                  // somethings gone wrong
        };

        // try to start the thread ( only possible if it is configured/connected )
        virtual bool Start( void ) = 0;

        /** @brief 
         *  
         *  @param pattern
         *  @param percentage
         * 
         *  @retval 
         * */
        bool setPattern(StatusLed::Pattern pattern, uint32_t percentage = 100);


        /** @brief Set the brightness of the visualization
         * 
         *  @param intensity brightness between 1 % and 100 %
         * */
        void setIntensity(uint32_t intensity);
        uint32_t getIntensity( void );

    protected:

        struct visualization {
            StatusLed::Pattern  Pattern;
            uint32_t            Percentage;
        } visualization;


    protected:
        cpp_freertos::Queue         m_Queue;
        cpp_freertos::MutexStandard m_Mutex;

        struct visualization        m_StatusPattern;
        struct visualization        m_ActualPattern;

        
        const uint32_t              m_TickLength;

        /** @brief 
         * 
         * */
        uint32_t                    m_StartTicks;
        uint32_t                    m_RemainingTicks;

        struct Settings
        {
            uint32_t                    Intensity;
        }                           m_Settings;

        void Run( void );

        /** @brief Updates the brightness of the connected LEDs
         * 
         * */
        virtual void updateIntensity( void ) = 0;

        /** @brief  Show the status pattern for a busy system
         *          
         * */
        virtual void showBusy( void ) = 0;
        virtual void showIdle( void ) = 0;        
        virtual void showTrackProgress( void ) = 0;
        virtual void showPaused( void ) = 0;
        virtual void showFault( void ) = 0;


        virtual uint32_t showError( void ) = 0;
        virtual uint32_t showOk( void ) = 0;           
        virtual uint32_t showPlaylistProgress( void ) = 0;
        virtual uint32_t showVolume( void ) = 0;
        virtual uint32_t showLocked( void ) = 0;        

    private:
    
        Preferences                 m_Preferences;

        /** @brief Load settings from NVS
         * 
         * */
        void loadSettings(void);
};

#endif