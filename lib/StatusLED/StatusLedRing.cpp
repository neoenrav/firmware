#include "StatusLedRing.h"

#include "Arduino.h"

#include "TimeElapsed.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "StatusLedDual";
#endif


//---------------------------------------------------------------------------------------
StatusLedRing::StatusLedRing():
    m_NumerberOfLeds(NUMBER_OF_LEDS)    
{
    FastLED.addLeds<CHIPSET , LED_PIN, COLOR_ORDER>(m_Leds, m_NumerberOfLeds).setCorrection( TypicalSMD5050 );
}


//---------------------------------------------------------------------------------------
StatusLedRing::~StatusLedRing()
{
    ESP_LOGD(TAG, "Destructor");
}


//---------------------------------------------------------------------------------------
bool StatusLedRing::Start( void )
{
    return StatusLed::Thread::Start();
}


//---------------------------------------------------------------------------------------
void StatusLedRing::updateIntensity( void )
{
    FastLED.setBrightness(m_Settings.Intensity & 0xFF);
}

//---------------------------------------------------------------------------------------
void StatusLedRing::showBusy( void )
{    
    const uint32_t timeslice = 1000 / (m_NumerberOfLeds / 4);

    uint32_t activeLED = ((TimeElapsed(m_StartTicks) % 1000) / timeslice);
    
    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
        {
            m_Leds[counter] = CRGB::Orange;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedRing::showIdle()
{
    const uint32_t speed = 1000;
    const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    uint32_t activeLED = ((runtime % speed) / timeslice);
    
    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
        {
            m_Leds[counter] = CRGB::Green;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedRing::showTrackProgress( void )
{
    uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if (counter <=  activeLEDs)
        {
            m_Leds[counter] = CRGB::Blue;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedRing::showPaused( void )
{
    uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if (counter <=  activeLEDs)
        {
            m_Leds[counter] = CRGB::Chocolate;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedRing::showFault( void )
{
    const uint32_t speed = 1000;
    const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    uint32_t activeLED = ((runtime % speed) / timeslice);
    
    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
        {
            m_Leds[counter] = CRGB::Red;
        }
        else
        {
            m_Leds[counter] = CRGB::IndianRed;
        }
    }

    FastLED.show();
}

//---------------------------------------------------------------------------------------
uint32_t StatusLedRing::showError()
{
    const uint32_t duration = 2000; // how long should the pattern be shown
    const uint32_t speed = 250;
    const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    uint32_t result = 0;
    int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    uint32_t activeLED = ((runtime % speed) / timeslice);
    
    // enable LEDs
    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
        {
            m_Leds[counter] = CRGB::Red;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();

    // lets check if the next tick is still within our timeframe
    if ((duration - runtime) > 0)
    {
        result = (duration - runtime);
    }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedRing::showOk( void )
{
    const uint32_t duration = 1000;

    int32_t result = 0;
    
    if (result < 0)
    {
        result = 0;
    }

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        m_Leds[counter] = CRGB::Green;
    }    

    FastLED.show();

    if (duration - TimeElapsed(m_StartTicks) > 0)
    {
        result = duration - TimeElapsed(m_StartTicks);
    }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedRing::showPlaylistProgress( void )
{
    const uint32_t duration = 5000;
    int32_t result = 0;

    uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if (counter <=  activeLEDs)
        {
            m_Leds[counter] = CRGB::GreenYellow;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();

    if (duration - TimeElapsed(m_StartTicks) > 0)
    {
        result = duration - TimeElapsed(m_StartTicks);
    }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedRing::showVolume( void )
{
    const uint32_t duration = 1500;
    int32_t result = 0;

    uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        if (counter <=  activeLEDs)
        {
            m_Leds[counter] = CRGB::Honeydew;
        }
        else
        {
            m_Leds[counter] = CRGB::Black;
        }
    }

    FastLED.show();

    if (duration - TimeElapsed(m_StartTicks) > 0)
    {
        result = duration - TimeElapsed(m_StartTicks);
    }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedRing::showLocked( void )
{
    const uint32_t duration = 2500;

    int32_t result = 0;
    
    if (result < 0)
    {
        result = 0;
    }

    for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    {
        m_Leds[counter] = CRGB::Pink;
    }    

    FastLED.show();

    if (duration - TimeElapsed(m_StartTicks) > 0)
    {
        result = duration - TimeElapsed(m_StartTicks);
    }

    return result;
}
