#ifndef _STATUS_LED_RING__H
    #define _STATUS_LED_RING__H

    #include "StatusLed.h"
    #include "thread.hpp"

    #define FASTLED_INTERNAL
    #include <FastLED.h>

    #define CHIPSET                     WS2812B     // type of Neopixel
    #define COLOR_ORDER                 GRB
    #define LED_PIN                     15
    #define NUMBER_OF_LEDS              12


class StatusLedRing: public StatusLed
{
    public:        
        StatusLedRing();
        ~StatusLedRing();

        // try to start the thread ( only possible if it is configured/connected )
        bool Start( void );   


        /*! \brief 
            *
            *   
            */        
        void setPattern(StatusLed::Pattern pattern, uint32_t percentage = 100);

        
        void setIntensity(uint32_t intensity);

    protected:
    

    private:
        
        const uint32_t      m_NumerberOfLeds;
        CRGB                m_Leds[NUMBER_OF_LEDS];

    private:

        void updateIntensity();

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showBusy( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showIdle( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showTrackProgress( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showPaused( void );

                /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showFault( void );



        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showError( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showOk( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showPlaylistProgress( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showVolume( void );
        
        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showLocked( void );

};

#endif