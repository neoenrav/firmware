#include "StatusLedRing.h"

#include "Arduino.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "StatusLedDual";
#endif


//---------------------------------------------------------------------------------------
StatusLed::StatusLed() : 
    Thread("StatusLed", 4096, tskIDLE_PRIORITY),
    m_Queue(2, sizeof(struct visualization)),
    m_Mutex(),
    m_TickLength(10)    
{
    //define the pattern to start with    
    m_StatusPattern.Pattern     = Pattern::BUSY;
    m_StatusPattern.Percentage  = 100;

    m_ActualPattern.Pattern     = Pattern::BUSY;
    m_ActualPattern.Percentage  = 100;

    m_RemainingTicks            = portMAX_DELAY;
}


//---------------------------------------------------------------------------------------
StatusLed::~StatusLed()
{

}


//---------------------------------------------------------------------------------------
void StatusLed::Run( void )
{
    struct visualization newPattern;

    //load "settings"
    loadSettings();

    updateIntensity();

    m_StartTicks = millis();    

    while(1)
    {

        // check for a new pattern
        if (m_Queue.Dequeue(&newPattern, m_TickLength))
        {
            // "reset" the timer
            m_StartTicks = millis();

            // set this as new status (permanent) pattern
            if ((newPattern.Pattern == Pattern::BUSY) ||
                (newPattern.Pattern == Pattern::IDLE) ||
                (newPattern.Pattern == Pattern::TRACK_PROGRESS) ||
                (newPattern.Pattern == Pattern::PAUSE) ||
                (newPattern.Pattern == Pattern::FAULT))
                {
                    m_StatusPattern.Pattern     = newPattern.Pattern;
                    m_StatusPattern.Percentage  = newPattern.Percentage;
                }

            m_ActualPattern.Pattern     = newPattern.Pattern;
            m_ActualPattern.Percentage  = newPattern.Percentage;

            m_RemainingTicks            = portMAX_DELAY;
        }
        
        if ((m_ActualPattern.Pattern != m_StatusPattern.Pattern) && (m_RemainingTicks < m_TickLength))
        {
            m_ActualPattern.Pattern     = m_StatusPattern.Pattern;
            m_ActualPattern.Percentage  = m_StatusPattern.Percentage;            
        }

        // this are the status pattern that do not timeout
        if (m_ActualPattern.Pattern == Pattern::IDLE)
        {
            showIdle();
        }
        else if (m_ActualPattern.Pattern == Pattern::BUSY) 
        {
            showBusy();
        }
        else if (m_ActualPattern.Pattern == Pattern::TRACK_PROGRESS) 
        {
            showTrackProgress();
        }
        else if (m_ActualPattern.Pattern == Pattern::PAUSE) 
        {
            showPaused();
        }
        else if (m_ActualPattern.Pattern == Pattern::FAULT) 
        {
            showFault();
        }

        //
        else if (m_ActualPattern.Pattern == Pattern::ERROR) 
        {
            m_RemainingTicks = showError();
        }
        else if (m_ActualPattern.Pattern == Pattern::OK) 
        {
            m_RemainingTicks = showOk();
        }
        else if (m_ActualPattern.Pattern == Pattern::PLAYLIST_PROGRESS) 
        {
            m_RemainingTicks = showPlaylistProgress();
        }
        else if (m_ActualPattern.Pattern == Pattern::VOLUME) 
        {
            m_RemainingTicks = showVolume();
        }
        else if (m_ActualPattern.Pattern == Pattern::LOCKED) 
        {
            m_RemainingTicks = showLocked();
        }
        else
        {
            log_e("Unknown Pattern %u", m_ActualPattern.Pattern);
        }
    }
}



//---------------------------------------------------------------------------------------
void StatusLed::setIntensity(uint32_t intensity)
{
    if (m_Settings.Intensity != intensity)
    {
        m_Mutex.Lock();
    
        m_Settings.Intensity = intensity;

        updateIntensity();

        m_Preferences.putULong("Intensity", m_Settings.Intensity);

        m_Mutex.Unlock();
    }
}

//---------------------------------------------------------------------------------------
bool StatusLed::setPattern(StatusLed::Pattern pattern, uint32_t percentage)
{
    bool result = false;

    if ((pattern != Pattern::EMPTY) && (percentage > 0) && (percentage <= 100))
    {
        struct visualization newPattern;
        
        newPattern.Pattern      = pattern;
        newPattern.Percentage   = percentage;

        result = m_Queue.Enqueue(&newPattern);
    }

    return result;
}

//---------------------------------------------------------------------------------------
void StatusLed::loadSettings(void)
{
    const uint32_t ActualVersion = 1;

    m_Preferences.begin("StatusLED", false);

    uint32_t version = m_Preferences.getULong("Version", 0);
    
    // load all settings (and default values)
    m_Settings.Intensity  = m_Preferences.getULong("Intensity", 20);

    // check the version number and update new settings
    switch (version)
    {
        case 0:
            m_Preferences.putULong("Intensity", m_Settings.Intensity);
        
//        case 1:
            // add new values and changed defaults here


            log_d("Updated StatusLed-Settings to v%u", ActualVersion);

            m_Preferences.putULong("Version", ActualVersion);

        case ActualVersion:
            // no need to update
            break;
        
        default:
            break;
    }
}