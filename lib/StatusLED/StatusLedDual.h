#ifndef _STATUS_LED_DUAL__H
    #define _STATUS_LED_DUAL__H

    #include "StatusLed.h"
    #include "thread.hpp"

class StatusLedDual: public StatusLed
{
    public:        
        StatusLedDual();
        ~StatusLedDual();

        // try to start the thread ( only possible if it is configured/connected )
        bool Start( void );   


        /*! \brief 
            *
            *   
            */        
        void setPattern(StatusLed::Pattern pattern, uint32_t percentage = 100);

        
        void setIntensity(uint32_t intensity);

    protected:
        


    private:

        void updateIntensity();

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showBusy( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showIdle( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showTrackProgress( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showPaused( void );

                /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        void showFault( void );



        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showError( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showOk( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showPlaylistProgress( void );

        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showVolume( void );
        
        /** @brief
         * 
         * @retval  true    -> if the pattern is finished
         *          false   -> 
         * */
        uint32_t showLocked( void );
    
};

#endif