#include "StatusLedDual.h"

#include "Arduino.h"

#include "TimeElapsed.h"

#ifdef ARDUINO_ARCH_ESP32
    #include "esp32-hal-log.h"    
#else
    static const char *TAG = "StatusLedDual";
#endif



//---------------------------------------------------------------------------------------
StatusLedDual::StatusLedDual()
{

}

//---------------------------------------------------------------------------------------
StatusLedDual::~StatusLedDual()
{

}


//---------------------------------------------------------------------------------------
bool StatusLedDual::Start( void )
{
    return StatusLed::Thread::Start();
}


//---------------------------------------------------------------------------------------
void StatusLedDual::updateIntensity( void )
{
    
}

//---------------------------------------------------------------------------------------
void StatusLedDual::showBusy( void )
{    
    // const uint32_t timeslice = 1000 / (m_NumerberOfLeds / 4);

    // uint32_t activeLED = ((TimeElapsed(m_StartTicks) % 1000) / timeslice);
    
    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
    //     {
    //         m_Leds[counter] = CRGB::Orange;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedDual::showIdle()
{
    // const uint32_t speed = 1000;
    // const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    // int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    // uint32_t activeLED = ((runtime % speed) / timeslice);
    
    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
    //     {
    //         m_Leds[counter] = CRGB::Green;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedDual::showTrackProgress( void )
{
    // uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if (counter <=  activeLEDs)
    //     {
    //         m_Leds[counter] = CRGB::Blue;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedDual::showPaused( void )
{
    // uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if (counter <=  activeLEDs)
    //     {
    //         m_Leds[counter] = CRGB::Chocolate;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();
}


//---------------------------------------------------------------------------------------
void StatusLedDual::showFault( void )
{
    // const uint32_t speed = 1000;
    // const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    // int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    // uint32_t activeLED = ((runtime % speed) / timeslice);
    
    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
    //     {
    //         m_Leds[counter] = CRGB::Red;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::IndianRed;
    //     }
    // }

    // FastLED.show();
}

//---------------------------------------------------------------------------------------
uint32_t StatusLedDual::showError()
{
    // const uint32_t duration = 2000; // how long should the pattern be shown
    // const uint32_t speed = 250;
    // const uint32_t timeslice = speed / (m_NumerberOfLeds / 4);
    
    uint32_t result = 0;
    // int32_t runtime = (TimeElapsed(m_StartTicks));    
    
    // uint32_t activeLED = ((runtime % speed) / timeslice);
    
    // // enable LEDs
    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if ((counter % (m_NumerberOfLeds / 4)) == activeLED)
    //     {
    //         m_Leds[counter] = CRGB::Red;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();

    // // lets check if the next tick is still within our timeframe
    // if ((duration - runtime) > 0)
    // {
    //     result = (duration - runtime);
    // }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedDual::showOk( void )
{
    // const uint32_t duration = 1000;

    int32_t result = 0;
    
    // if (result < 0)
    // {
    //     result = 0;
    // }

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     m_Leds[counter] = CRGB::Green;
    // }    

    // FastLED.show();

    // if (duration - TimeElapsed(m_StartTicks) > 0)
    // {
    //     result = duration - TimeElapsed(m_StartTicks);
    // }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedDual::showPlaylistProgress( void )
{
    // const uint32_t duration = 5000;
    int32_t result = 0;

    // uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if (counter <=  activeLEDs)
    //     {
    //         m_Leds[counter] = CRGB::GreenYellow;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();

    // if (duration - TimeElapsed(m_StartTicks) > 0)
    // {
    //     result = duration - TimeElapsed(m_StartTicks);
    // }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedDual::showVolume( void )
{
    // const uint32_t duration = 5000;
    int32_t result = 0;

    // uint32_t activeLEDs = (uint32_t) ((m_NumerberOfLeds-1) * (m_ActualPattern.Percentage / 100.0));

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     if (counter <=  activeLEDs)
    //     {
    //         m_Leds[counter] = CRGB::Honeydew;
    //     }
    //     else
    //     {
    //         m_Leds[counter] = CRGB::Black;
    //     }
    // }

    // FastLED.show();

    // if (duration - TimeElapsed(m_StartTicks) > 0)
    // {
    //     result = duration - TimeElapsed(m_StartTicks);
    // }

    return result;
}


//---------------------------------------------------------------------------------------
uint32_t StatusLedDual::showLocked( void )
{
    // const uint32_t duration = 2500;

    int32_t result = 0;
    
    // if (result < 0)
    // {
    //     result = 0;
    // }

    // for (uint32_t counter = 0; counter < m_NumerberOfLeds; counter++)
    // {
    //     m_Leds[counter] = CRGB::Pink;
    // }    

    // FastLED.show();

    // if (duration - TimeElapsed(m_StartTicks) > 0)
    // {
    //     result = duration - TimeElapsed(m_StartTicks);
    // }

    return result;
}


// StatusLed::StatusLed(uint8_t red_1, uint8_t green_1, uint8_t red_2, uint8_t green_2 ):
//     Thread("LED", 1024, tskIDLE_PRIORITY),
//     m_Queue(5, sizeof(StatusLedPattern))
// {

// //    ledcSetup(m_channel_Red_1,      m_frequency, m_resolution);
//     ledcSetup(m_channel_Green_1,    m_frequency, m_resolution);
//     ledcSetup(m_channel_Red_2,      m_frequency, m_resolution);
//     ledcSetup(m_channel_Green_2,    m_frequency, m_resolution);

//     ledcAttachPin(red_1,    m_channel_Red_1);
//     ledcAttachPin(green_1,  m_channel_Green_1);
//     ledcAttachPin(red_2,    m_channel_Red_2);
//     ledcAttachPin(green_2,  m_channel_Green_2);
// }

// StatusLed::~StatusLed(void)
// {

// }

// typedef struct LedPattern_s
// {
//     struct TimeSlice_s
//     {
//         const uint8_t Led_1_Red;
//         const uint8_t Led_1_Green;
//         const uint8_t Led_2_Red;
//         const uint8_t Led_2_Green;
//     } const Slice[8];
// } LedPattern_s;

// const LedPattern_s LedPattern[1] = 
// { 
//     //Standby
//     {   .Slice =
//             {
//                 // 0
//                 {
//                     .Led_1_Red      = 32,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 0,
//                     .Led_2_Green    = 0
//                 },
//                 // 1
//                 {
//                     .Led_1_Red      = 32,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 0,
//                     .Led_2_Green    = 0
//                 },
//                 // 2
//                 {
//                     .Led_1_Red      = 32,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 0,
//                     .Led_2_Green    = 0
//                 },
//                 // 3
//                 {
//                     .Led_1_Red      = 32,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 0,
//                     .Led_2_Green    = 0
//                 },
//                 // 4
//                 {
//                     .Led_1_Red      = 0,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 32,
//                     .Led_2_Green    = 0
//                 },
//                 // 5
//                 {
//                     .Led_1_Red      = 0,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 32,
//                     .Led_2_Green    = 0
//                 },
//                 // 6
//                 {
//                     .Led_1_Red      = 0,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 32,
//                     .Led_2_Green    = 0
//                 },
//                 // 7
//                 {
//                     .Led_1_Red      = 0,
//                     .Led_1_Green    = 0,
//                     .Led_2_Red      = 32,
//                     .Led_2_Green    = 0
//                 },
//             }
//     }

// };

// void StatusLed::Run()
// {

//     while (1)
//     {
//         StatusLedPattern newPattern;

//         //check if we've got a new pattern
//         if (m_Queue.Dequeue(&newPattern, m_SliceTimeMs/portTICK_PERIOD_MS ) == true)
//         {
//             Pattern newTempPattern = newPattern.GetPattern();
            
            
            
//             if (newTempPattern == Pattern::Standby)
//             {
//                 m_ActualPattern = newTempPattern;
//                 m_Countdown     = 0;

//                 ESP_LOGD(TAG, "New Pattern: Standby");
//             }
//             else if (newTempPattern == Pattern::Progress)
//             {
//                 m_BackUpPatern  = m_ActualPattern;
//                 m_ActualPattern = newTempPattern;

//                 m_Countdown     = 5000;
                
//                 ESP_LOGD(TAG, "New Pattern: Progress");
//             }
//             else
//             {
//                 ESP_LOGD(TAG, "Unknown Pattern");
//             }

//             // reset the slice counter
//             m_Counter = 0;
//         }
      
//         // check if we are on a temporarelly pattern        
//         if (m_Countdown >= m_SliceTimeMs) 
//         {
//             m_Countdown -= m_SliceTimeMs;
//         }
//         // if the time is over, restore the old pattern
//         else if (m_Countdown > 0) 
//         {
//             m_Countdown = 0;
//             m_Counter   = 0;
//             m_ActualPattern = m_BackUpPatern;
//         }

//         //show the actual slice of the pattern
//         ledcWrite(m_channel_Red_1, LedPattern[0].Slice[m_Counter].Led_1_Red);
//         ledcWrite(m_channel_Green_1, LedPattern[0].Slice[m_Counter].Led_1_Green);
//         ledcWrite(m_channel_Red_2, LedPattern[0].Slice[m_Counter].Led_2_Red);
//         ledcWrite(m_channel_Green_2, LedPattern[0].Slice[m_Counter].Led_2_Green);  

//         // increment the counter
//         m_Counter++;
//         if (m_Counter > 7)
//         {
//             m_Counter = 0;
//         }

//         //StatusLed::DelayMS(500);
//     }
// }

