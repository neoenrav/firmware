# Network Library

This library should handle all network connections for the player

## General

The general flow of the task in this modules looks like

![General Flow Diagram][flowdiagram]

Depending on the given settings, the task will create a own AP (with mDSN) 
to allow clients an easy connection to the player.

The Webserver could be used to configure and control the player.

The FTP Server is used to upload new files.

The MQTT server could be used to control the player via house automatization
or simply collect statistics.

## Specific Servers and Functions 

### WLAN Access Point and mDNS 



### WLAN Client

Connect to an existing 2.4GHz WLAN


### OTA Service

Allow wireless firmware updates

### WebServer


### FTP Server


### MQTT Server




[flowdiagram]: Network.png "Flowchart"