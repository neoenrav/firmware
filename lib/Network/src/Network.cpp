#include "Network.h"
#include "FormatBytes.h"
#include "TimeElapsed.h"

#include <SPIFFS.h>

#include <WiFi.h>
#include <WebServer.h>
#include <DNSServer.h>


#ifdef ARDUINO_ARCH_ESP32
#include "esp32-hal-log.h"
#else
static const char *TAG = "Network";
#endif

const byte DNS_PORT = 53;

ArduinoOTAClass *pArduinoOTA = NULL;
WebServer       *pWebServer = NULL;
Network         *pNetwork;

//---------------------------------------------------------------------------------------
Network::Network(/* args */) : Thread("Network", 8192, tskIDLE_PRIORITY),
                                m_Verbose(true),
                                m_LastAccessTime(0),                                
                                m_Update(false),
                                m_Preferences(),
                                m_pWebserver(NULL),
                                m_pDnsServer(NULL),
                                m_pFtpServer(NULL),
                                m_pArduinoOTA(NULL),
                                m_StatusChangeMutex()
{
    pNetwork = this;
}

//---------------------------------------------------------------------------------------
Network::~Network()
{
    stopAllServices();
}

//---------------------------------------------------------------------------------------
bool Network::Start(void)
{
    bool result = false;

    result = Thread::Start();

    return result;
}

//---------------------------------------------------------------------------------------
void Network::Run(void)
{

    m_Preferences.begin("WLAN");
    
    loadSettings();    

    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);


    // if the network is off, we could ignore all possible settings
    if (m_Settings.Mode == NetworkMode::OFF)
    {
        m_NetworkStatus = NetworkStatus::OFF;
    }
    else
    {
        m_NetworkStatus = NetworkStatus::UPDATE;
    }

    //
    while(1)
    {        

        if (m_NetworkStatus == NetworkStatus::OFF)
        {
            DelayMS(100);

            if (m_Update)
            {
                m_Update = false;

                m_NetworkStatus = NetworkStatus::UPDATE;
            }
        }
        else {

            // disable "everything"
            if (m_NetworkStatus == NetworkStatus::SHUTDOWN)
            {
                stopAllServices();

                m_NetworkStatus = NetworkStatus::OFF;
            }

            //update network settings
            else if (m_NetworkStatus == NetworkStatus::UPDATE)
            {
                //if the update is a  complete disable
                if (m_Settings.Mode == NetworkMode::OFF)
                {
                    m_NetworkStatus = NetworkStatus::SHUTDOWN;
                }
                else
                {
                    IPAddress apIP(192, 168, 1, 1);

                    // prevent any configurations changes until we are finished with this run
                    m_StatusChangeMutex.Lock();

                    // start the AP or connect to an existing network
                    if (WiFi.getMode() == WIFI_OFF)
                    {

                        WiFi.disconnect();
                        WiFi.mode(WIFI_OFF);

                        // check for AP or STA mode
                        if (m_Settings.Password.isEmpty() || m_Settings.SSID.isEmpty())
                        {
                            WiFi.mode(WIFI_AP);
                            WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
                            WiFi.softAP("EnRav AP");

                            DelayMS(2000);

                            if (m_Verbose)
                            {
                                log_i("EnRav AP ready with IP %s", apIP.toString().c_str());
                            }
                        }
                        else
                        {
                            uint32_t connectionAttempt = 1;
                            
                            while ((WiFi.status() != WL_CONNECTED) && (connectionAttempt <= 5)) 
                            {
                                WiFi.mode(WIFI_STA);

                                WiFi.begin(m_Settings.SSID.c_str(), m_Settings.Password.c_str());
                                                
                                if (m_Verbose)
                                {
                                    log_d("Trying to connect to %s (Attempt %d)", m_Settings.SSID.c_str(), connectionAttempt);
                                }

                                DelayMS(500);

                                // if we are still not connected
                                if (WiFi.status() != WL_CONNECTED)
                                {
                                    connectionAttempt++;

                                    WiFi.disconnect();
                                    WiFi.mode(WIFI_OFF);

                                    DelayMS(500);
                                }
                            }
                            
                            if (WiFi.status() == WL_CONNECTED)
                            {
                                if (m_Verbose)
                                {                
                                    log_i("Connected to \"%s\", IP %s", m_Settings.SSID.c_str(), WiFi.localIP().toString().c_str());
                                }
                            }
                        }
                    }

                    // if the connect / start was successfull, we could start the servers
                    if (((WiFi.getMode() == WIFI_STA) && (WiFi.status() == WL_CONNECTED)) || (WiFi.getMode() == WIFI_AP))
                    {
                        // the mDNS is only needed in AP mode
                        if (WiFi.getMode() == WIFI_AP)
                        {
                            if (m_pDnsServer == NULL)
                            {
                                startDnsServer(apIP);
                            }
                        }
                        else
                        {
                            if (m_pDnsServer)
                            {
                                stopDnsServer();
                            }
                        }

                        // OTA should alsways be enabled if the WiFi is active
                        startArduinoOTA();

                        // the webserver is needed when enabled or for the access point mode
                        if ((m_Settings.WebServer.Enabled) || (WiFi.getMode() == WIFI_AP))
                        {    
                            if (m_pWebserver == NULL)
                            {
                                startWebserver();
                            }
                        }
                        else
                        {
                            if (m_pWebserver)
                            {
                                stopWebserver();
                            }
                        }

                        // check for the ftp server
                        if (m_Settings.FTP.Enabled)
                        {
                            if (m_pFtpServer == NULL)
                            {

                            }
                        }
                        else
                        {
                            if (m_pFtpServer)
                            {

                            }
                        }
                    
                        //update the access timestamp
                        m_LastAccessTime = millis();

                        m_NetworkStatus = NetworkStatus::RUNNING;
                    }
                    
                    // someting went wrong
                    else
                    {
                        log_e("Failed to connect / create WiFi");

                        m_NetworkStatus = NetworkStatus::SHUTDOWN;
                    }
                    
                    m_StatusChangeMutex.Unlock();

                } // Mode not OFF
            }

            // run the network
            else if (m_NetworkStatus == NetworkStatus::RUNNING)
            {
                DelayMS(10);

                // if the wifi is not enabled (permanent), it will be disabled after some time
                if (m_Settings.Mode == NetworkMode::AUTO)
                {
                    uint32_t timestamp = m_LastAccessTime;

                    if (TimeElapsed(timestamp) > m_Settings.TimeOut)
                    {
                        log_i("Shutting down network after %umin of inactivity", m_Settings.TimeOut / (1000 * 60));
                        m_NetworkStatus = NetworkStatus::SHUTDOWN;
                    }
                }

                if (m_Update)
                {
                    m_Update = false;

                    m_NetworkStatus = NetworkStatus::UPDATE;
                }
            }

            
            // check for the enebaled servers
            if (m_pDnsServer)
            {
                m_pDnsServer->processNextRequest();
            }

            if (m_pWebserver)
            {
                m_pWebserver->handleClient();
            }

            if (m_pFtpServer)
            {
                m_pFtpServer->handleFTP();
            }

            if (m_pArduinoOTA)
            {
                m_pArduinoOTA->handle();
            }
        }
    }
}


//---------------------------------------------------------------------------------------
void Network::loadSettings(void)
{
    const uint32_t ActualVersion = 4;

    uint32_t version = m_Preferences.getULong("Version", 0);
        
    // load all settings (and default values)
    m_Settings.Mode     = static_cast<NetworkMode>(m_Preferences.getInt("Mode", static_cast<int32_t>(NetworkMode::AUTO)));    
    m_Settings.TimeOut  = m_Preferences.getULong("Timeout", 10U * 60 * 1000);

    m_Settings.SSID     = m_Preferences.getString("SSID", "");
    m_Settings.Password = m_Preferences.getString("Password", "");

    // check the version number and update new settings
    switch (version)
    {
        case 0:
        case 1:
            m_Preferences.putString("SSID", "");
            m_Preferences.putString("Password", "");

        case 2: 
            m_Preferences.putULong("Timeout", 10U * 60 * 1000);

        case 3:
            m_Preferences.remove("Enable");
            m_Preferences.putInt("Mode", static_cast<int32_t>(NetworkMode::AUTO));

        // always update the version as last value
            m_Preferences.putULong("Version", ActualVersion);
            break;
        
        case ActualVersion:
            //nothing to update here
            break;

        default:
            break;
    }    
}


//---------------------------------------------------------------------------------------
void Network::sendTrigger(void)
{
    //
    m_Update = true;
}


//---------------------------------------------------------------------------------------
bool Network::getActive(void)
{
    bool result = false;

    if (m_NetworkStatus != NetworkStatus::OFF)
    {
        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void Network::setTimeout(uint32_t timeout)
{
    uint32_t newTimeOut = timeout * (60 * 1000U);

    if (m_Settings.TimeOut != newTimeOut)
    {
        m_Preferences.putULong("Timeout", newTimeOut);

        m_StatusChangeMutex.Lock();
        m_Settings.TimeOut = newTimeOut;
        m_StatusChangeMutex.Unlock();
    }
}


//---------------------------------------------------------------------------------------
uint32_t Network::getTimeout(void)
{
    return m_Settings.TimeOut / (60 * 1000U);
}


//---------------------------------------------------------------------------------------
void Network::setMode(Network::NetworkMode newMode)
{
    if (m_Settings.Mode != newMode)
    {
        m_Preferences.putInt("Mode", static_cast<int32_t>(newMode));

        m_StatusChangeMutex.Lock();
        m_Settings.Mode = newMode;
        m_StatusChangeMutex.Unlock();

        m_Update = true;
    }
}


//---------------------------------------------------------------------------------------
Network::NetworkMode Network::getMode(void)
{
    return m_Settings.Mode;
}


//---------------------------------------------------------------------------------------
void Network::setSSID(String& ssid)
{
    if (!ssid.equals(m_Settings.SSID))
    {

        m_Preferences.putString("SSID", ssid);

        m_StatusChangeMutex.Lock();
        m_Settings.SSID = ssid;
        m_StatusChangeMutex.Unlock();       

        m_Update = true;
    }
}


//---------------------------------------------------------------------------------------
String& Network::getSSID(void)
{
    return m_Settings.SSID;
}


//---------------------------------------------------------------------------------------
void Network::setWifiPassword(String& password)
{
    if (!password.equals(m_Settings.Password))
    {

        m_Preferences.putString("Password", password);

        m_StatusChangeMutex.Lock();
        m_Settings.Password = password;
        m_StatusChangeMutex.Unlock();

        m_Update = true;
    }
}


//---------------------------------------------------------------------------------------
String& Network::getWifiPassword(void) 
{
    return m_Settings.Password;
}


//---------------------------------------------------------------------------------------
void Network::tranferFile(const char *path)
{
    if (SPIFFS.exists(path))
    {
        File file = SPIFFS.open(path);
        String content = getContentType(path);

        pWebServer->streamFile(file, content);

        file.close();
    }
}


//---------------------------------------------------------------------------------------
String Network::getContentType(String filename) {
    if (filename.endsWith(".htm")) 
    {
        return "text/html";
    } 
    else if (filename.endsWith(".html")) 
    {
        return "text/html";
    }
    else if (filename.endsWith(".css")) 
    {
        return "text/css";
    }
    else if (filename.endsWith(".js")) 
    {
        return "application/javascript";
    } 
    else if (filename.endsWith(".png")) 
    {
        return "image/png";
    } 
    else if (filename.endsWith(".gif")) 
    {
        return "image/gif";
    } 
    else if (filename.endsWith(".jpg")) 
    {
        return "image/jpeg";
    }
    else if (filename.endsWith(".ico")) 
    {
        return "image/x-icon";
    }
    else if (filename.endsWith(".xml")) 
    {
        return "text/xml";
    }
    else if (filename.endsWith(".pdf")) 
    {
        return "application/x-pdf";
    } 
    else if (filename.endsWith(".zip")) 
    {
        return "application/x-zip";
    }
    else if (filename.endsWith(".gz")) 
    {
        return "application/x-gzip";
    }

    return "text/plain";
}


//---------------------------------------------------------------------------------------
void Network::stopAllServices( void )
{
    stopArduinoOTA();

    stopDnsServer();

    stopWebserver();

    stopFtpServer();

    stopMqttServer();

    // make sure the wifi is no longer connected
    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);
}

//---------------------------------------------------------------------------------------
bool Network::startDnsServer(const IPAddress &resolvedIP)
{
    bool result = false;

    if (m_pDnsServer == NULL)
    {
        m_pDnsServer = new DNSServer;

        // if DNSServer is started with "*" for domain name, it will reply with
        // provided IP to all DNS request
        m_pDnsServer->start(DNS_PORT, "*", resolvedIP);

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void Network::stopDnsServer(void)
{
    if (m_pDnsServer)
    {
        m_pDnsServer->stop();

        delete m_pDnsServer;
        m_pDnsServer = NULL;
    }
}


//---------------------------------------------------------------------------------------
bool Network::startWebserver(void)
{
    bool result = false;

    if(m_pWebserver == NULL)
    {
        // check if the necessary html pages exists
        if (!SPIFFS.begin() ||  !SPIFFS.exists("/captive.html"))
        {
            //
            log_e("Not all HTML files exist in SPIFF, aborting webserver start!");
        }
        else
        {
            m_pWebserver = new WebServer(80);

            // allow the callbacks to access the webserver
            pWebServer = m_pWebserver;

            // all captive pages are "equal"
            m_pWebserver->on("/restart", HTTP_GET, [] () {
                if (pWebServer)
                {
                    pWebServer->send(200, "text/html", "ESP is rebooting...");                        
                    ESP.restart();
                }
            });

            m_pWebserver->on("/init", HTTP_POST, [] () {
                if (pWebServer)
                {
                    //update the access timestamp
                    pNetwork->m_LastAccessTime = millis();

                    if ((pWebServer->hasArg("ssid")) && (pWebServer->hasArg("pwd")))
                    {                        
                        String ssid = pWebServer->arg("ssid");
                        String pwd  = pWebServer->arg("pwd");

                        log_d("Got WLAN Credentials ssid: %s pwd %s", ssid, pwd);

                        pNetwork->setSSID(ssid);
                        pNetwork->setWifiPassword(pwd);
                    }

                    pNetwork->tranferFile("/captive.html");
                }
            });

            m_pWebserver->onNotFound([]() {
                if (pWebServer)
                {
                    //update the access timestamp
                    pNetwork->m_LastAccessTime = millis();

                    pNetwork->tranferFile("/captive.html");
                }
                else
                {
                    log_e("Webserver not configured correctly!");
                }
            });

            m_pWebserver->begin();

            result = true;
        }
    }

    return result;
}


//---------------------------------------------------------------------------------------
void Network::stopWebserver(void)
{
    if (m_pWebserver)
    {
        m_pWebserver->stop();

        delete m_pWebserver;
        m_pWebserver = NULL;
        pWebServer = NULL;

        SPIFFS.end();
    }
}


//---------------------------------------------------------------------------------------
bool Network::startArduinoOTA(void)
{
    bool result = false;

    if (m_pArduinoOTA == NULL)
    {
        m_pArduinoOTA = new ArduinoOTAClass();

        pArduinoOTA = m_pArduinoOTA;

        m_pArduinoOTA->onStart([]() 
        {
            if (pArduinoOTA)
            {
                String type;

                if (pArduinoOTA->getCommand() == U_FLASH)
                {
                    type = "sketch";
                }
                else // U_SPIFFS
                {
                type = "filesystem";
                }

                // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                if (pWebServer)
                {
                    pNetwork->stopWebserver();
                }

                log_i("Start updating %s", type.c_str());
            }
        });

        m_pArduinoOTA->onEnd([]() 
        {
            log_i("OTA End");
        });

        m_pArduinoOTA->onProgress([](unsigned int progress, unsigned int total) 
        {
            //update the access timestamp
            pNetwork->m_LastAccessTime = millis();

            log_d("Progress: %u%%", (progress / (total / 100)));
        });

        m_pArduinoOTA->onError([](ota_error_t error) 
        {
            if (error == OTA_AUTH_ERROR)
            {
                log_d("Auth Failed");
            }
            else if (error == OTA_BEGIN_ERROR)
            {
                log_d("Begin Failed");
            }
            else if (error == OTA_CONNECT_ERROR) {
                log_d("Connect Failed");
            }
            else if (error == OTA_RECEIVE_ERROR) 
            {
                log_d("Receive Failed");
            }
            else if (error == OTA_END_ERROR) 
            {
                log_d("End Failed");
            }
        });

        m_pArduinoOTA->begin();

        result = true;
    }

    return result;
}


//---------------------------------------------------------------------------------------
void Network::stopArduinoOTA(void)
{
    if (m_pArduinoOTA)
    {
        m_pArduinoOTA->end();
        delete m_pArduinoOTA;
        m_pArduinoOTA = NULL;
        pArduinoOTA = NULL;
    }
}


//---------------------------------------------------------------------------------------
bool Network::startFtpServer(void)
{
    bool result = false;

    return result;
}


//---------------------------------------------------------------------------------------
void Network::stopFtpServer(void)
{

}


//---------------------------------------------------------------------------------------
bool Network::startMqttServer(void)
{
    bool result = false;

    return result;
}


//---------------------------------------------------------------------------------------
void Network::stopMqttServer(void)
{

}
