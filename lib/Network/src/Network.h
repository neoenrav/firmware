/** @file Network.h
 * 
 * @brief Handle all Network connections for the player
 * 
 * 
 * Mode OFF - Wifi is completely disabled
 * Mode AUTO - Enabled after start up and trigger
 * Mode ENABLED - Permanently enabled
 * 
 */ 

#ifndef NETWORK_H
    #define NETWORK_H

    #include "Arduino.h"
    #include <Preferences.h>

    #include <WebServer.h>
    #include <DNSServer.h>
    #include <ArduinoOTA.h>

    #include "ESP32FtpServer.h"

    #include "thread.hpp"
    #include "mutex.hpp"

    /*! \brief Brief description.
        *         Brief description continued.
        *
        *  Detailed description starts here.
        */
    class Network: public cpp_freertos::Thread
    {
        public:

            bool                    m_Verbose;

            uint32_t                m_LastAccessTime;            

            enum class              NetworkMode {
                OFF,                                    /// @brief The network feature is completely inactive
                AUTO,                                   /// @brief The network is enabled after power up or a trigger command and disabled when the defined timeout elapsed
                ENABLED                                 /// @brief the network is permanently enabled
            };

        public:
            Network();
            ~Network();

            // try to start the thread ( only possible if it is configured/connected )
            bool        Start( void );   

            //----------------------------------------
            // 
            //----------------------------------------
            /**
             * @brief Use this command to (re-)start or update the WiFi/Network module 
             * */
            void        sendTrigger(void);            

            /** @brief Query if the network module is active at the moment
             *          Ignoring why the network was enabled
             * 
             *  @retval true    - network is active
             *          false   - network is "sleeping"
             * */
            bool        getActive(void);

            void        setMode(NetworkMode newMode);
            NetworkMode getMode(void);

            void        setTimeout(uint32_t timeout);
            uint32_t    getTimeout(void);

            void        setSSID(String& ssid);
            String&     getSSID(void);

            void        setWifiPassword(String& password);
            String&     getWifiPassword(void);


            String      getContentType(String filename);
            void        tranferFile(const char *path);

        protected:
        
            void        Run( void );                                // the "real" thread function

          
        private:
            enum class              NetworkStatus {
                OFF,
                UPDATE,
                RUNNING,
                SHUTDOWN
            }                       m_NetworkStatus;

            

            bool                    m_Update;

            Preferences             m_Preferences;            

            ///@brief

            struct Settings
            {
                NetworkMode             Mode;
                uint32_t                TimeOut;

                String                  SSID;
                String                  Password;
                struct MQTT {
                    bool                    Enabled;
                    String                  Server;                    
                    String                  User;
                    String                  Password;
                }                       MQTT;
                struct WebServer {
                    bool                    Enabled;
                    uint32_t                Port;
                }                       WebServer;

                struct FTP {
                    bool                    Enabled;
                    uint32_t                Port;
                    String                  User;
                    String                  Password;
                }                       FTP;                
            }                       m_Settings;

            // our different servers / 
            WebServer                   *m_pWebserver;
            DNSServer                   *m_pDnsServer;
            FtpServer                   *m_pFtpServer;
            ArduinoOTAClass             *m_pArduinoOTA;


            cpp_freertos::MutexStandard  m_StatusChangeMutex;

        private:
            void                    loadSettings(void);

            void                    stopAllServices( void );

            bool                    startDnsServer(const IPAddress &resolvedIP);
            void                    stopDnsServer(void);

            bool                    startWebserver(void);
            void                    stopWebserver(void);

            bool                    startArduinoOTA(void);
            void                    stopArduinoOTA(void);

            bool                    startFtpServer(void);
            void                    stopFtpServer(void);

            bool                    startMqttServer(void);
            void                    stopMqttServer(void);

    };
    
#endif
        