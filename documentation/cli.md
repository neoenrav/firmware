# **C**ommand **L**ine **I**nterface

## Commands

- [System](#system--general)
  - [help]
  - [version]
  - [heap](#heap)
  - [ls](#ls)
  - [cd](#cd)
  - [cat](#cat)
- [Player](#player)
  - [volume](#volume)
- [Wifi](#wifi)

   
## System / General

All of these command belong to the main system of the player and provide information   
about the actual system state and general help to maintain / debug the player.

### Heap

`heap?` - query the available and the free heap memory of the system

#### Synopsis

#### Description

#### Example

    /$ heap?
    Heap: 215kB of 328kB free


### LS

`ls` - 

#### Synopsis

#### Description

#### Example


### CD

`cd` - 

#### Synopsis

#### Description

#### Example


### CAT

`cat` - 

#### Synopsis

#### Description

#### Example

## Wifi

## Player

### Volume

`volume?` - query the 
`volume g`

#### Synopsis

#### Description

#### Example

    /$ heap?
    Heap: 215kB of 328kB free


### Name

`name` - 

#### Synopsis

#### Description

#### Example